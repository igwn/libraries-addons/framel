# Version used during development
cmake_minimum_required(VERSION 3.20)
project(${PROJECT_NAME}/Tests)

# Include custom CMake modules and utilities
file(GLOB CMAKE_MODULE_PATHS "${CMAKE_CURRENT_SOURCE_DIR}/../cmake/*")
foreach(MODULE_PATH ${CMAKE_MODULE_PATHS})
    if(IS_DIRECTORY ${MODULE_PATH})
        list(PREPEND CMAKE_MODULE_PATH ${MODULE_PATH})
    endif()
endforeach()

include(MessageColor)        # Include utility for colored message output
include(FindPackageStandard) # Include utility for finding packages
include(ProjectArchitecture) # Include utility for project architecture
include(ParentCMakeOnly)     # Include utility for parent CMake only actions
include(Tests)               # Include utility for loading tests

# Output section header for Tests
message_title("Tests")

FILE_SOURCES(SOURCES "${CMAKE_CURRENT_SOURCE_DIR}" ABSOLUTE)
DUMP_ARCHITECTURE(SOURCES RELATIVE_PATH "${CMAKE_SOURCE_DIR}")
GET_COMMON_PATH(SOURCEPATH_COMMON SOURCES)

if(SOURCES)
    add_custom_target(tests ALL
        COMMAND ${CMAKE_CTEST_COMMAND} --rerun-failed --output-on-failure $<$<BOOL:$ENV{VERBOSE}>:--verbose>
        COMMENT "Running tests"
    )
endif()

#
# Looking for script files
foreach( SRC ${SOURCES} )

    get_filename_component(SOURCE ${SRC} NAME_WE)
    get_filename_component(SOURCEPATH ${SRC} PATH)

    file(RELATIVE_PATH SOURCEPATH_REL "${SOURCEPATH_COMMON}" "${SOURCEPATH}")
    if(NOT "${SOURCEPATH_REL}" STREQUAL "")
        list(APPEND SOURCEPATH_REL "/")
    endif()

    message_color("-- Preparing target C++ `./${SOURCEPATH_REL}${SOURCE}`" COLOR GREEN)

    # Prepare test program
    add_gtest(${SOURCE} ${SRC})
    target_link_package(${SOURCE} ${LIBRARY})
    target_include_directories(${SOURCE} PUBLIC "${CMAKE_CURRENT_SOURCE_DIR}/include")

    # Custom `make tests`
    add_dependencies(tests ${SOURCE})
    
    # Install test files to specified location with permissions
    install(FILES ${CMAKE_CURRENT_BINARY_DIR}/${SOURCE}
            DESTINATION share/tests
            PERMISSIONS OWNER_READ OWNER_WRITE OWNER_EXECUTE GROUP_READ GROUP_EXECUTE WORLD_READ WORLD_EXECUTE
            OPTIONAL
    )
    
endforeach()

