#pragma once 

#include <string>
#include <vector>
#include <typeindex>
#include <variant>

namespace FrameFormat {

        enum class FrType
        {
                FrVect   , FrTable   , FrDetector,
                FrRawData, FrProcData, FrSimData , FrStatData,
                FrEvent  , FrSimEvent, FrSummary ,
                FrAdcData, FrSerData , FrHistory , FrMsg
        };

        struct TFrAbstract { };
        struct TFrVect: public TFrAbstract
        {
                std::string name;
                short compress;

                enum class TYPE { 
                        FR_VECT_C = 0, FR_VECT_2S = 1, FR_VECT_8R = 2, FR_VECT_4R = 3, 
                        FR_VECT_4S = 4, FR_VECT_8S = 5, FR_VECT_8C = 6, FR_VECT_16C = 7, 
                        FR_VECT_STRING = 8, FR_VECT_2U = 9, FR_VECT_4U = 10,FR_VECT_8U = 11,
                        FR_VECT_1U = 12 
                };
                TYPE type;

                unsigned long nData;
                
                unsigned long nBytes;
                std::vector<double> data; // NB: double is not optimized, but allow to plot using TBrowser and minimize operations..

                unsigned int nDim;
                std::vector<unsigned int> nx;
                std::vector<double> dx;
                std::vector<double> startX;
                std::vector<std::string> unitX;

                std::string unitY;
        };

        struct TFrTable: public TFrAbstract
        {
                std::string name;
                std::string comment;
                unsigned short nColumn;
                unsigned int nRow;

                std::vector<std::string> columnName;
                TFrVect column;
        };

        struct TFrAdcData: public TFrAbstract
        {
                std::string name;
                std::string comment;
                unsigned int channelGroup;
                unsigned int channelNumber;

                unsigned int nBits;
                float bias;
                float slope;
                std::string units;

                double sampleRate;
                double timeOffset;
                double fShift;
                float phase;

                unsigned short dataValid;

                TFrVect data;
                TFrVect aux;
        };

        struct TFrDetector: public TFrAbstract
        {
                std::string name;
                char  prefix[2];

                double longitude;
                double latitude;
                float elevation;

                float armXazimuth;
                float armYazimuth;

                float armXaltitude;
                float armYaltitude;

                float armXmidpoint;
                float armYmidpoint;

                int localTime;

                // Presently undefined
                TFrVect aux;
                // Presently undefined
                TFrTable table;
        };

        struct TFrEvent: public TFrAbstract
        {
                std::string name;
                std::string comment;
                std::string inputs;

                unsigned int GTimeS;
                unsigned int GTimeN;
                float timeBefore;
                float timeAfter;
                unsigned int eventStatus;

                float amplitude;
                float probability;
                std::string statistics;

                unsigned int nParam;
                std::vector<double> parameters;
                std::vector<std::string> parameterNames;

                TFrVect data;
                TFrTable table;
        };

        struct TFrHistory: public TFrAbstract
        {
                std::string name;
                unsigned int time;
                std::string comment;
        };

        struct TFrMsg: public TFrAbstract
        {
                std::string alarm;
                std::string message;
                unsigned int severity;
                unsigned int GTimeS;
                unsigned int GTimeN;
        };

        struct TFrProcData: public TFrAbstract
        {
                std::string name;
                std::string comment;

                enum class TYPE { Unknown = 0, TimeSeries = 1, FrequencySeries = 2, OtherSeries = 3, TimeFrequency = 4, Wavelets = 5, MultiDimensional = 6 };
                TYPE type;

                enum class SUBTYPE { Unknown = 0, DFT = 1, ASD = 2, PSD = 3, CSD = 4, Coherence = 5, TF = 6};
                SUBTYPE subType;

                double timeOffset;
                double tRange;
                double fShift;
                float phase;
                double fRange;
                double BW;

                unsigned short nAuxParam;
                std::vector<double> auxParam;
                std::vector<std::string> auxParamNames;

                TFrVect data;
                TFrVect aux;
                TFrTable table;
                TFrHistory history;
        };

        struct TFrSerData: public TFrAbstract
        {
                std::string name;
                unsigned int timeSec;
                unsigned int timeNsec;
                double sampleRate;

                std::string data;
                TFrVect serial;
                TFrTable table;
        };

        struct TFrRawData: public TFrAbstract
        {
                std::string name;

                TFrMsg logMsg;
                TFrVect more;
        };

        struct TFrSimData: public TFrAbstract
        {
                std::string name;
                std::string comment;

                double sampleRate;
                double timeOffset;
                double fShift;
                float phase;

                TFrVect data;
                TFrVect input;
                TFrTable table;
        };

        struct TFrSimEvent: public TFrAbstract
        {
                std::string name;
                std::string comment;
                std::string inputs;

                unsigned int GTimeS;
                unsigned int GTimeN;
                float timeBefore;
                float timeAfter;
                float amplitude;

                unsigned int nParam;
                std::vector<double> parameters;
                std::vector<std::string> parameterNames;
                TFrVect data;
                TFrTable table;
        };

        struct TFrStatData: public TFrAbstract
        {
                std::string name;
                std::string comment;
                std::string representation;

                unsigned int timeStart;
                unsigned int timeEnd;
                unsigned int version;

                TFrDetector detector;
                TFrVect data;
                TFrTable table;
        };

        struct TFrSummary: public TFrAbstract
        {
                std::string name;
                std::string comment;
                std::string test;

                unsigned int GTimeS;
                unsigned int GTimeN;

                TFrVect moments;
                TFrTable table;
        };

        using TFrAbstractT = std::variant<
                FrameFormat::TFrAdcData*, 
                FrameFormat::TFrDetector*,
                FrameFormat::TFrEvent*,
                FrameFormat::TFrHistory*,
                FrameFormat::TFrMsg*,
                FrameFormat::TFrProcData*,
                FrameFormat::TFrRawData*,
                FrameFormat::TFrSerData*,
                FrameFormat::TFrSimData*,
                FrameFormat::TFrSimEvent*,
                FrameFormat::TFrStatData*,
                FrameFormat::TFrSummary*,
                FrameFormat::TFrTable*,
                FrameFormat::TFrVect*
        >;
}
