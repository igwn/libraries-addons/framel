#pragma once

#include "FrameFormat/Abstract/TFrameAbstract.h"
#include "FrameFormat/TFrameProxy.h"

namespace FrameFormat {

        using GPS = std::pair<double,double>;

        class TFrame;
        class TFrameManager;
        class TFrameChain: public TFrameAbstract
        {
                protected:

                        std::mutex mtx;
                        
                        friend class TFrameManager;

                        TFrameManager *fManager = NULL;
                        int fileID = 0;
                        int frameID = 0;

                        int padWidth = 892;
                        int padHeight = 512;
                         
                        using TObject::Dump;
                        using TObject::Print;
                        using TObject::Write;

                        TString fName;
                        std::vector<TString> fileNames;
                        std::vector<std::vector<TFrame*>> files;
                        std::vector<GPS> fileGPS;

                        std::vector<double> fileFrameLength;
                        int compression = 0;

                public:

                        TFrameChain(const char *fName, Option_t *option = "", TString tags = "*", std::vector<FrType> dataClass = {}, int decimate = 1, std::vector<int> channelGroups = {}, std::vector<int> channelNumbers = {}, Verbosity verbosity = Verbosity::NORMAL);
                        TFrameChain(std::vector<TString> fName, Option_t *option = "", TString tags = "*", std::vector<FrType> dataClass = {}, int decimate = 1, std::vector<int> channelGroups = {}, std::vector<int> channelNumbers = {}, Verbosity verbosity = Verbosity::NORMAL);
                        ~TFrameChain() { Close(); };
                        
                        TFrameChain(std::vector<TFrame*> frames, Option_t *option = "", TString tags = "*", std::vector<FrType> dataClass = {}, int decimate = 1, std::vector<int> channelGroups = {}, std::vector<int> channelNumbers = {});

                        bool IsOpen();
                        void Close();

                        inline TFrameManager &GetManager() { return *this->fManager; }
                        TString GetFileName(int fileID = -1);
                        int GetNFrames(int fileID = -1);
                        int GetNFiles();

                        GPS GetGPS(int fileID = -1, int frameID = -1);
                        double GetDuration(int fileID = -1, int frameID = -1);

                        int GetFileID(int globalID);
                        std::vector<int> GetFileID(std::vector<int> globalID);
                        int GetFrameID(int globalID);
                        std::vector<int> GetFrameID(std::vector<int> globalID);
                        
                        int GetGlobalID(int fileID, int localID);
                        int GetGlobalID(TFrame *frame);

                        TFrame    *GetFrame(int globalID = -1);
                        TFrame    *GetFrame(int fileID, int frameID);

                        inline int GetCompression() { return this->compression; }
                        void SetCompression(int compression) { this->compression = compression; }

                        void Free();

                        std::vector<int> Select          (TString formula, std::vector<double> params = {});
                        std::vector<int> SelectByFrame(std::vector<int> globalIDs);
                        std::vector<int> SelectByFrame(int firstGlobalID = -1, int lastGlobalID = -1);
                        std::vector<int> SelectByGPS     (std::vector<double> gps);
                        std::vector<int> SelectByGPS     (double firstGPS = -1, double lastGPS = -1);
                        std::vector<int> SelectByMaxDuration(double duration);
                        std::vector<int> SelectByMinDuration(double duration);
                        std::vector<int> SelectByDQ      (unsigned int dataQuality);

                        void Print(Option_t *opt = "", int globalID = -1);
                        void Print(Option_t *opt, std::vector<int> globalIDs);

                        inline void SetPadHeight(int padHeight) { this->padHeight = padHeight; }
                        inline void SetPadWidth(int padWidth) { this->padWidth = padWidth; }

                        void DumpTOC();
                        void DumpTOC(int globalID);

                        bool AddFriend(TFrameChain *c);
                        void RemoveFriend(TFrameChain *c);

                        void Dump(std::vector<int> globalIDs);
                        void Dump(int globalID = -1);

                        int Write (TString filename, Option_t *opt = "update", int framesPerFile = 1, std::vector<int> globalIDs = {});
                        TFrameChain *Reshape(TString filename, int dt, Option_t *opt = "", std::vector<int> globalIDs = {});
                        TFrameChain *Trend  (TString filename, int fs, Option_t *opt = "", std::vector<int> globalIDs = {});

                        bool IsContiguous();

                        TObject* GetObject(TString objName, std::vector<int> globalIDs = {});
                        TObject* GetObject(TString objName, TString branchName, Option_t *option = "", std::vector<int> globalIDs = {});
                        TFrame * GetFrame(GPS);

                protected:

                        int WriteGWF(TString name, Option_t *opt = "", std::vector<int> globalIDs = {});
                        int WriteROOT(TString name, Option_t *opt = "", std::vector<int> globalIDs = {});
                        static int WriteROOT(TFile *f, TFrame *frame);

                ClassDef(TFrameChain,1);
        };

        using TFrameFile = TFrameChain;
}
