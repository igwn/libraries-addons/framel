# Include custom standard package
list(PREPEND CMAKE_MODULE_PATH ${CMAKE_CURRENT_LIST_DIR}/../Modules)
include(FindPackageStandard)

if(NOT ROOT_FOUND)
    message(STATUS "Load ROOT before calling ROOT+.")
endif()

# Load using standard package finder
find_package_standard(
    NAMES RIOPlus
    HEADERS "Rioplus.h"
    PATHS ${ROOTPLUS} ${ROOTPLUS_DIR} $ENV{ROOTPLUS} $ENV{ROOTPLUS_DIR}
)
