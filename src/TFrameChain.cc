/**
 **********************************************
 *
 * \file TFrameChain.cc
 * \brief Source code of the TFrameChain class
 * \author Marco Meyer \<marco.meyer@cern.ch\>
 *
 *********************************************
 */

#include "FrameFormat/TFrameChain.h"
#include "FrameFormat/TFrameManager.h"
#include "FrameFormat/TFrame.h"
ClassImp(FrameFormat::TFrameChain)

#include "FrameFormat/Impl/TFrameHeader.h"
#include <TFileReader.h>

// @TODO: Implement decimate
// @TODO: Implement trend
// @TODO: Implement reshape

namespace FrameFormat {

    TFrameChain::TFrameChain(std::vector<TString> fName, Option_t *option, TString tags, std::vector<FrType> dataClass, int decimate, std::vector<int> channelGroups, std::vector<int> channelNumbers, Verbosity verbosity)
               : TFrameChain(ROOT::IOPlus::Helpers::Implode(" ", fName), option, tags, dataClass, decimate, channelGroups, channelNumbers, verbosity) { }

    TFrameChain::TFrameChain(const char *fName, Option_t *option, TString tags, std::vector<FrType> dataClass, int decimate, std::vector<int> channelGroups, std::vector<int> channelNumbers, Verbosity verbosity)
               : fName(fName)
    {
        this->SetVerbosity(verbosity);
        this->SetDebug(verbosity);

        this->fName = ROOT::IOPlus::Helpers::Implode(" ", TFileReader::RecursiveExpandTextFile(fName));
        this->fName = ROOT::IOPlus::Helpers::Implode(" ", TFileReader::ExpandWildcardUrl(ROOT::IOPlus::Helpers::Explode(" ", this->fName)));
        
        FrFile *fMaster = FrFileINew(this->fName);
        if(fMaster == NULL) return;

        TString opt = TString(option);
                opt.ToLower();

        std::vector<std::vector<GPS>> _frameGPS;
        std::vector<std::vector<unsigned int>> _frameDQ;

        bool keepDuplicates = opt.Contains("d");
        std::vector<TString> _fileNames = ROOT::IOPlus::Helpers::Explode(" ", this->fName);
        std::vector<int> _fileAllocation;
        int fileID = -1;
        do {

            fileID = fileID + 1;
            
            FrFileH *file = fMaster->current;
            if(file == NULL) continue;
            
            if(!TPrint::IsSuperQuiet()) {

                TPrint::SetSingleCarriageReturn();
                TPrint::ProgressBar(Form("Reading TOC file #%d", fileID+1), "", fileID+1, _fileNames.size());
            }

            FrTOC *toc = FrTOCReadFull(fMaster); // Just to know the number of frames contained in current file
            if(toc == NULL) {

                TPrint::Warning(__METHOD_NAME__, Form("Failed to read TOC file #%d", fileID+1));
                continue; // Failed to load file.. @TODO an alternative loading procedure if TOC not found
            }

            TString _fileName = TString(file->fileName);
            GPS _fileGPS = std::make_pair(file->tStart, file->tStart+file->length);
            double _fileFrameLength = *toc->dt;

            bool bFileFound = std::find(this->fileNames.begin(), this->fileNames.end(), _fileName) != this->fileNames.end();
            bool bGpsFound  = std::find(this->fileGPS.begin(), this->fileGPS.end(), _fileGPS) != this->fileGPS.end();
            if(!keepDuplicates && (bFileFound || bGpsFound)) {

                TPrint::Warning(__METHOD_NAME__, Form("Duplicates found for file #%d.. skip `%s`", fileID+1, _fileName.Data()));
                continue;
            }

            this->files.push_back(std::vector<TFrame*>(toc->nFrame, NULL));
            this->fileNames.push_back(_fileName);
            this->fileGPS.push_back(_fileGPS);

            _frameGPS.push_back(std::vector<GPS>(toc->nFrame));
            _frameDQ.push_back(std::vector<unsigned int>(toc->nFrame));
            for(int frameID = 0; frameID < toc->nFrame; frameID++) {
                
                _frameGPS[fileID][frameID].first  = gClockGPS->Timestamp(toc->GTimeS[frameID], toc->GTimeN[frameID]);
                _frameGPS[fileID][frameID].second = _frameGPS[fileID][frameID].first + toc->dt[frameID];
                _frameDQ [fileID][frameID] = toc->dataQuality[frameID];
            }

            this->fileFrameLength.push_back(*toc->dt);
            _fileAllocation.push_back(toc->nFrame);

        } while( FrFileINext(fMaster, 0., 0., NULL, FR_NO) == 0 );

        // Reorder vectors
        std::vector<int> indices(this->fileGPS.size());
        std::iota(indices.begin(), indices.end(), 0);
        std::sort(indices.begin(), indices.end(), [&](int A, int B) -> bool { return fileGPS[A].first < fileGPS[B].first; });

        TFileReader::Reorder(this->files,     indices);
        TFileReader::Reorder(this->fileNames, indices);
        TFileReader::Reorder(this->fileGPS,   indices);
        TFileReader::Reorder(this->fileFrameLength,    indices);
        TFileReader::Reorder(_fileAllocation, indices);

        TFileReader::Reorder(_frameGPS, indices);
        TFileReader::Reorder(_frameDQ,  indices);

        // Open new master
        this->fName = ROOT::IOPlus::Helpers::Implode(" ", this->fileNames);
        FrFileIEnd(fMaster);

        fMaster = FrFileINew(this->fName);
        this->fManager = new TFrameManager(*fMaster, _fileAllocation);

        // Open reordered master file
        for(int fileID = 0, nFiles = this->files.size(); fileID < nFiles; fileID++) {

            for(int frameID = 0, nFrames = this->files[fileID].size(); frameID < nFrames; frameID++) {
                this->files[fileID][frameID] = new TFrame(this, fileID, frameID, tags, dataClass, decimate, channelGroups, channelNumbers);
                this->files[fileID][frameID]->SetGPS(_frameGPS[fileID][frameID]);
                this->files[fileID][frameID]->SetDataQuality(_frameDQ[fileID][frameID]);
            }
        }
    };

    void TFrameChain::Free()
    {
         for(int fileID = 0, nFiles = this->files.size(); fileID < nFiles; fileID++) {

            for(int frameID = 0, nFrames = this->files[fileID].size(); frameID < nFrames; frameID++) {
                this->files[fileID][frameID]->Free();
            }
        }
    }

    bool TFrameChain::IsOpen()
    {
        return this->fManager != NULL && (&this->fManager->GetMaster()) != NULL;
    }

    void TFrameChain::Close()
    {
        for(int i = 0, I = this->files.size(); i < I; i++) {

            for(int j = 0, J = this->files[i].size(); j < J; j++) {

                delete this->files[i][j];
            }
        }

        this->files.clear();
        this->fileFrameLength.clear();
        this->fileGPS.clear();
        this->fileNames.clear();

        if(this->fManager != NULL) { 

            FrFile *fMaster = &this->fManager->GetMaster();
            if(fMaster != NULL) {    
                FrFileIEnd(fMaster);
            }
             
            delete this->fManager;
            this->fManager = NULL;
        }
    }

    bool TFrameChain::AddFriend(TFrameChain *c)
    {
        if(c == NULL) return false;
        if (this->GetNFrames() != c->GetNFrames()) {

            std::cerr << "Failed to chain `" << this->fName << "` and `" << c->fName << "`: same number of frame expected" << std::endl;
            return false;
        }

        std::vector<TFrame *> siblings;
        for(int i = 0, N = this->GetNFrames(); i < N; i++) {
            
            TFrame *f1 = this->GetFrame(this->GetFileID(i), this->GetFrameID(i));
            GPS GPSrange = f1->GetGPS();

            TFrame *f2 = c->GetFrame(f1->GetGPS());
            if (f2 == NULL) {

                std::cerr << "Failed to chain `" << this->fName << "` and `" << c->fName << "`: GPS [" << GPSrange.first << "," << GPSrange.second << "] not found" << std::endl;
                return false;
            }
            
            siblings.push_back(f2);
        }
        
        for(int i = 0, N = this->GetNFrames(); i < N; i++) {
            TFrame *f1 = this->GetFrame(this->GetFileID(i), this->GetFrameID(i));
            f1->AddFriend(siblings[i]);
        }

        return true;
    }

    void TFrameChain::RemoveFriend(TFrameChain *c)
    {
        // std::vector<TFrame *> siblings;
        // for(int i = 0, N = this->GetNFrames(); i < N; i++) {
            
        //     TFrame *f1 = this->GetFrame();
        //     TFrame *f2 = c->GetFrame(f1->GetGPS());
        //     if (f2 == NULL) {

        //         std::cerr << "Failed to chain `" << this->fName << "` and `" << c->fName << "`: GPS [" << GPSrange.first << "," << GPSrange.second << "] not found" << std::endl;
        //         return false;
        //     }
            
        //     siblings.push_back(f2);
        // }
        
        // for(int i = 0, N = this->GetNFrames(); i < N; i++) {
        //     TFrame *f1 = this->GetFrame(this->GetFileID(i), this->GetFrameID(i));
        //             f1->AddFriend(siblings[i]);
        // }
        std::cout << "TODO" << std::endl;
    }

    void TFrameChain::Print(Option_t *opt, int globalID)
    {
        std::vector<int> globalIDs;
        for(int i = 0, N = this->GetNFrames(); i < N; i++) {
            globalIDs.push_back(i);
        }

        return Print(opt, globalID < 0 ? globalIDs : std::vector<int>({globalID}));
    }

    void TFrameChain::Print(Option_t *opt, std::vector<int> globalIDs)
    {
        TString option = opt;
                option.ToLower();

        bool bRead = option.Contains("read");
        bool bDump = option.Contains("dump");
        bool bWait = option.Contains("wait");
        bool bTOC  = option.Contains("toc");

        bool bInteractive = option.Contains("interactive");

        if(bTOC) this->DumpTOC();    

        TVirtualPad *gPadBak = gPad;
        int padWidth  = this->padWidth;
        int padHeight = this->padHeight;

        int globalID_first = globalIDs.size() ? *std::min_element(globalIDs.begin(), globalIDs.end()) : 0;
        for(int globalID = globalID_first, globalN = this->GetNFrames(); globalID < globalN; globalID++)
        {
            bool isValid = std::find(globalIDs.begin(), globalIDs.end(), globalID) != globalIDs.end();
            bool bDraw = option.Contains("draw");
            bool bDrawLimit = false;

            int fileID  = this->GetFileID(globalID);
            int frameID = this->GetFrameID(globalID);
            
            TString fileName = this->GetFileName(fileID);
            int nFiles = this->files.size();
            int nFrames = this->files[fileID].size();

            GPS GPSrange(0,0);
            if(bInteractive) {

                gPrint->ClearPage();

                TFrame *frame = this->files[fileID][frameID];
                        frame->Print("interactive");

                if (GPSrange.first == GPSrange.second)
                    GPSrange = frame->GetGPS();

                if(isValid) std::cout << ">>> selectable frame: " << TPrint::kGreen << "✓" << TPrint::kNoColor;
                else std::cout << TPrint::kOrange << ">>> skipped in selection: " << TPrint::kRed << "✗" << TPrint::kNoColor;
                if(isValid) std::cout << std::endl << std::endl;
                else std::cout << std::endl;
                
                enum class MyAction { PrevFrame, NextFrame, PrevFile, NextFile, PrevBatch, NextBatch, ReadTOC, ReadFrame, Quit };
                std::map<TKeyboard::KeyEvent, MyAction> keyEvents;
                keyEvents[TKeyboard::KeyEvent({27, 91, 68})] = MyAction::PrevFrame; // LEFT
                keyEvents[TKeyboard::KeyEvent({27, 91, 67})] = MyAction::NextFrame; // RIGHT
                keyEvents[TKeyboard::KeyEvent({27, 91, 49, 59, 50, 68})] = MyAction::PrevBatch; // Shift+LEFT
                keyEvents[TKeyboard::KeyEvent({27, 91, 49, 59, 50, 67})] = MyAction::NextBatch; // Shift+RIGHT
                keyEvents[TKeyboard::KeyEvent({27, 91, 65})] = MyAction::PrevFile;  // UP
                keyEvents[TKeyboard::KeyEvent({27, 91, 66})] = MyAction::NextFile;  // DOWN
                keyEvents[TKeyboard::KeyEvent({9})         ] = MyAction::ReadTOC;   // TAB
                keyEvents[TKeyboard::KeyEvent({10})        ] = MyAction::ReadFrame; // ENTER
                keyEvents[TKeyboard::KeyEvent({27})        ] = MyAction::Quit     ; // ESC

                std::vector<std::vector<unsigned char>> sequences;
                for( auto event : keyEvents)
                    sequences.push_back(event.first.GetSequence());
                
                if(gPadBak) gPadBak->cd(1);

                bool first = true;
                while(1) {

                    if(!first) std::cout << std::endl;

                    TString fileStr  = nFiles > 1 ? "[UP|DOWN] to change file; " : "";
                    TString frameStr = nFrames > 1 ? "[LEFT|RIGHT] to change frame (use shift to skip faster); " : "";
                    TString readableStr = isValid ? "[TAB] to read TOC and [ESC] to exit" : "Press [ESC] to exit";
                    TString blinkingStr = isValid ? "Press [ENTER] to " + TString(first ? "read" : "re-read") + " the frame." : "";

                    int keyPressed = first && frame->GetN() <= 5 ? true : TPrint::WaitForKeyPress(__METHOD_NAME__, sequences,
                        blinkingStr, fileStr + frameStr + readableStr); 

                    first = false;
                    if(gPadBak) gPadBak->cd();

                    bool bTOC  = false;
                    bool bRead = false;
                    bool bInteractiveReset = true;

                    int nObjects = frame->GetN();
                    auto it = keyEvents.find(TKeyboard::KeyEvent(sequences[keyPressed]));
                    switch(it->second) {

                        case MyAction::PrevFile:
                            globalID = TMath::Max(0, this->GetGlobalID(TMath::Max(0,fileID-1), 0));
                            if(globalID < 0) globalID = 0;
                            break;

                        case MyAction::NextFile:
                            globalID = TMath::Min(globalN, this->GetGlobalID(TMath::Min(nFiles, fileID+1), 0));
                            break;
                        
                        case MyAction::PrevBatch:
                            globalID = TMath::Max(0, globalID - 32);
                            break;

                        case MyAction::NextBatch:
                            globalID = TMath::Min(globalN, globalID + 32);
                            break;

                        case MyAction::PrevFrame: [[fallthrough]];
                            globalID = TMath::Max(0, globalID-1);
                            break;

                        case MyAction::Quit:
                            globalID = globalN+1;
                            break;

                        case MyAction::ReadFrame:
                            bInteractiveReset = false;
                            bRead = true;
                            break;

                        case MyAction::ReadTOC:
                            bInteractiveReset = false;
                            bTOC = true;
                            break;

                        case MyAction::NextFrame: [[fallthrough]];
                        default:
                            globalID = TMath::Min(globalN, globalID+1);
                            break;
                    }

                    if(bInteractiveReset) {

                        if(globalID < 0) globalID = 0;
                        else if(globalID == globalN) globalID = globalID-1; // Prevent to exit when reaching the last frame.
                        
                        globalID--; // Compensate for loop index increase.
                        break;
                    }

                    if(!isValid) continue;

                    if(bTOC) this->DumpTOC(this->GetGlobalID(fileID,0));
                    if(bRead) {

                        TFrame *frame = this->files[fileID][frameID];
                                frame->Print();

                        std::vector<TFrameProxyT> proxies = frame->ReadProxy();

                        for(int k = 0, K = proxies.size(); k < K; k++) {

                            std::visit([&bDraw, &bDump, &bDrawLimit, &GPSrange, &nObjects, &gPadBak, &padWidth, &padHeight](auto* obj) -> void { 

                                for(int basket = 0, B = obj->GetNBaskets(); basket < B; basket++) {

                                    obj->Print(basket);
                                    if(gPrint->IsDebugEnabled(10) || bDump)
                                        obj->Dump(basket);

                                    if(bDraw) 
                                        gSystem->ProcessEvents();

                                    if(bDraw && !bDrawLimit) {

                                        int iPad = ROOT::IOPlus::Helpers::GetPadIndex(gPad)+1;
                                        if (iPad > 0) {

                                            int nPads = ROOT::IOPlus::Helpers::CountPads(gPad->GetMother());
                                            if (iPad >= nPads) {

                                                gPrint->Warning(__METHOD_NAME__, "Too many objects to draw.");
                                                bDrawLimit = true;

                                                continue;
                                            }

                                            gPad->cd(iPad+1);
                                        }

                                        TTree *tree = (TTree *) obj->GetObject(basket);
                                        if(tree == NULL) continue;

                                        TH1 *hData = TFrameProxyAbstract::Histogram(tree, obj->GetBranchName(), "delete");
                                        if (hData) {

                                            hData->GetXaxis()->SetTimeOffset(GPSrange.first);
                                            hData = gClock->FormatTimeAxis(hData);

                                            if(hData->GetEntries() > 16384) {
                                                gPrint->Warning(__METHOD_NAME__, "Histogram is too large, reduced performances expected for '" + TString(hData->GetName()) + "'.");
                                            }

                                            if(gPad) gPad->GetMother()->cd(iPad+1);
                                            else {

                                                int maxPads = int(gPicasso->GetDisplayHeight()/padHeight);
                                                int nPads = TMath::Min(maxPads, nObjects);
                                                TCanvas *c = TCanvas::MakeDefCanvas();
                                                         c->SetWindowSize(padWidth, padHeight*nPads);
                                                
                                                gPadBak = gPad;

                                                gPad->Divide(1, nPads);
                                                gPad->cd(1);
                                            } 

                                            hData->Draw();
                                            if (gPad) {

                                                gPad->Modified();
                                                gPad->Update();
                                            }
                                        }
                                   }
                                }

                            }, proxies[k]);

                            if(k % 32 == 0 && k > 0) {

                                gPrint->PressAnyKeyToContinue(__METHOD_NAME__, "Next object(s) will be read.");
                            }
                        }

                        if (gPad) {

                            gPad->Modified();
                            gPad->Update();
                        }
                    }
                }

                if(gPadBak) gPadBak->cd();

            } else {

                if(std::find(globalIDs.begin(), globalIDs.end(), globalID) == globalIDs.end())
                    continue;

                TString fileName = this->GetFileName(fileID);
                int nFrame = this->files[fileID].size();

                TString header = TPrint::kGreen + "Master #" + TString::Itoa(fileID+1,10) + "/" + TString::Itoa(nFiles,10) + TPrint::kNoColor + "; " + TPrint::MakeItShorter(fileName, 64) + "; " + TPrint::kRed + TString::Itoa(nFrames,10) + " frame" + (nFrame > 1 ? "s" : "") + TPrint::kNoColor;
                        header += TPrint::kNoColor + " <<< from " + TPrint::kOrange + gClockGPS->UTC(fileGPS[fileID].first) + TPrint::kNoColor + " to " + TPrint::kOrange + gClockGPS->UTC(fileGPS[fileID].second) + TPrint::kNoColor;

                if(bDraw) {
                    gPrint->Warning(__METHOD_NAME__, "Drawing is only permitted in interactive mode.");
                }

                if(globalID == globalID_first || frameID == 0) {

                    if(bTOC) {

                        if (gPrint->IsDebugEnabled(bTOC ? 10 : 100)) {

                            std::cout << header << std::endl;
                            gPrint->PressAnyKeyToContinue(__METHOD_NAME__, "Table of content will be read.");
                            gPrint->ClearPage();
                        }

                        std::cout << header << std::endl;
                        this->DumpTOC(globalID);
                    }
                }

                TFrame *frame = this->files[fileID][frameID];
                frame->Print();

                if(bRead) {

                    std::vector<TFrameProxyT> proxies = frame->ReadProxy();
                    for(int k = 0, K = proxies.size(); k < K; k++) {

                        std::visit([&bDump](auto* obj) -> void { 

                            for(int basket = 0, B = obj->GetNBaskets(); basket < B; basket++) {
                                obj->Print(basket);
                                if(gPrint->IsDebugEnabled(10) || bDump)
                                    obj->Dump(basket);
                            }

                        }, proxies[k]);

                        if(k % 32 == 0 && k > 0) {

                            gPrint->PressAnyKeyToContinue(__METHOD_NAME__, "Next object(s) will be read.");
                        }
                    }
                }

                if(bRead) {

                    gPrint->PressAnyKeyToContinue(bWait ? 1 : 10, __METHOD_NAME__, "To go to next frame..");
                    if (gPrint->IsDebugEnabled(bWait ? 1 : 10)) gPrint->ClearPage();
                }
            }
        }
    }

    int TFrameChain::GetNFrames(int fileID)
    {
        if(fileID < 0) {

            int total = 0;
            for(int i = 0, N = this->GetNFiles(); i < N; i++)
                total += this->GetNFrames(i);

            return total;
        }

        return fileID < this->files.size() ? this->files[fileID].size() : 0;
    }

    int TFrameChain::GetGlobalID(TFrame *frame)
    {
        return this->GetGlobalID(frame->GetFileID(), frame->GetFrameID());
    }

    int TFrameChain::GetNFiles()
    {
        return this->files.size();
    }

    TString TFrameChain::GetFileName(int fileID)
    {
        if(fileID < 0 || fileID >= fileNames.size())
            throw std::invalid_argument("Unexpected file ID ("+TString::Itoa(fileID,10)+") provided.");
    
        return fileNames[fileID];
    }

    std::vector<int> TFrameChain::GetFileID(std::vector<int> globalIDs)
    {
        std::vector<int> fileIDs;
        for(int i = 0, N = globalIDs.size(); i < N; i++)
        {
            int fileID = this->GetFileID(globalIDs[i]);
            if(fileID < 0) continue;
            
            fileIDs.push_back(fileID);
        }

        return fileIDs;
    }

    std::vector<int> TFrameChain::GetFrameID(std::vector<int> globalIDs)
    {
        std::vector<int> frameIDs;
        for(int i = 0, N = globalIDs.size(); i < N; i++)
        {
            int frameID = this->GetFrameID(globalIDs[i]);
            if(frameID < 0) continue;
            
            frameIDs.push_back(frameID);
        }

        return frameIDs;
    }

    void TFrameChain::DumpTOC()
    {
        std::cout   << "==> Table of content at:" << static_cast<void*>(this)
                        << ", name=\"" << TPrint::MakeItShorter(TString(this->fName)) << "\""
                        << ", class=\"" << this->Class() << "\""<< std::endl;

        if(this->IsOpen()) std::cout   << "    This chain contains " << this->GetNFrames() << " frame(s) out of " << this->GetNFiles() << " file(s)" << std::endl;
        else {
                std::cout << "    File closed." << std::endl;
                return;
        }

        std::cout << std::endl;
    }

    void TFrameChain::DumpTOC(int globalID)
    {
        this->GetManager().DumpTOC(&this->GetManager().GetMaster(), this->GetFileID(globalID));
        std::cout << std::endl << std::flush;
    }

    void TFrameChain::Dump(int globalID)
    {
        std::vector<int> globalIDs;
        for(int i = 0, N = this->GetNFrames(); i < N; i++) {
            globalIDs.push_back(i);
        }

        return Dump(globalID < 0 ? globalIDs : std::vector<int>({globalID}));
    }

    bool TFrameChain::IsContiguous()
    {
        int nFiles = this->GetNFiles();
        if(nFiles == 0) return false;

        TFrame *frame = this->GetFrame(0,0);
        if(frame == NULL) return false;

        do {

            if(!frame->IsContiguous()) return false;

        } while( (frame = frame->Next() ));

        return true;
    }

    void TFrameChain::Dump(std::vector<int> globalIDs)
    {
        std::cout   << "==> Dumping object at:" << static_cast<void*>(this)
                    << ", name=\"" << TString(this->fName) << "\""
                    << ", class=\"" << this->Class() << "\""<< std::endl;

        TString contiguous = !this->IsContiguous() ? TPrint::kRed + " [DISCONTINUED]" + TPrint::kNoColor : "";
        std::cout   << "    This object contains in total " << this->GetNFrames() << " frame(s), out of " << this->GetNFiles() << " file(s)" << contiguous << std::endl;

        for(int fileID = 0, nFiles = this->GetNFiles(); fileID < nFiles; fileID++)
        {
            //
            // TABLE OF CONTENT
            {
                TString frameID = "==========  " + TPrint::kNoColor + "TABLE OF CONTENT #"+ TString::Itoa(fileID+1,10) + TPrint::kGreen+"  ==========";
                TString spacer = ROOT::IOPlus::Helpers::Spacer(frameID.Length()-11, '=');

                std::cout << TPrint::kGreen << std::endl;
                std::cout << spacer << std::endl;
                std::cout << frameID << std::endl;
                std::cout << spacer << TPrint::kNoColor << std::endl;

                std::cout << std::endl;
                std::cout << "==> Table of content at:" << static_cast<void*>(this)
                          << ", name=\"" << TString(this->fName) << "\""
                          << ", class=\"" << this->Class() << "\""<< std::endl;

                this->GetManager().ReadTOC(&this->GetManager().GetMaster(), fileID);
            }

            //
            // DUMPING ALL FRAMES
            for(int frameID = 0, nFrames = this->GetNFrames(fileID); frameID < nFrames; frameID++) {

                int globalID = this->GetGlobalID(fileID, frameID);
                if(std::find(globalIDs.begin(), globalIDs.end(), globalID) == globalIDs.end())
                    continue;

                std::cout << std::endl << "==> Reading frame #" << (frameID+1) << " at:" << static_cast<void*>(this) << std::endl;
                TFrame *frame = this->GetFrame(fileID, frameID);
                if(frame) frame->Dump();
            }

            //
            // END OF FILE
            //
            {
                TString frameID = "==========  " + TPrint::kNoColor + "END OF FILE #"+ TString::Itoa(fileID+1,10) + TPrint::kOrange+"  ==========";
                TString spacer = ROOT::IOPlus::Helpers::Spacer(frameID.Length()-11, '=');
                if(this->GetNFrames(fileID) < 1) continue;

                std::cout << TPrint::kOrange << std::endl;
                std::cout << spacer << std::endl;
                std::cout << frameID << std::endl;
                std::cout << spacer << TPrint::kNoColor << std::endl;

                FrEndOfFile *eof = this->GetManager().ReadEOF(&this->GetManager().GetMaster(), fileID);
                delete eof; // Delete it right away.. we just want to set the cursor at the right place and display debug information
            }
        }
    }

    int TFrameChain::GetFileID(int globalID)
    {
        if(globalID < 0) return this->fManager->GetFileID(&this->fManager->GetMaster());

        for(int i = 0, N = this->GetNFiles(); i < N; i++) {

            int maxLocalID = this->files[i].size();
            if(globalID < maxLocalID) return i;

            globalID -= maxLocalID;
        }

        return -1;
    }

    int TFrameChain::GetFrameID(int globalID)
    {
        if(globalID < 0) return this->fManager->GetFrameID(&this->fManager->GetMaster());

        for(int i = 0, N = this->files.size(); i < N; i++) {

            int maxLocalID = this->files[i].size();
            if(globalID < maxLocalID) return globalID;

            globalID -= maxLocalID;
        }

        return -1;
    }

    int TFrameChain::GetGlobalID(int fileID, int localID)
    {
        int globalID = 0;
        int maxGlobalID = 0;

        for(int i = 0, N = this->files.size(); i < N; i++) {

            maxGlobalID += this->files[i].size();
            if(i < fileID)
                globalID += this->files[i].size();
        }

        globalID += localID;

        return globalID < maxGlobalID ? globalID : -1;
    }

    TFrame* TFrameChain::GetFrame(GPS GPSrange)
    {
        for(int i = 0, N = this->GetNFrames(); i < N; i++) {
            
            TFrame *f = this->GetFrame(this->GetFileID(i), this->GetFrameID(i));
            GPS _GPSrange = f->GetGPS();

            if(GPSrange.first != _GPSrange.first) continue;
            if(GPSrange.second != _GPSrange.second) continue;

            return f;
        }

        return NULL;
    }

    TFrame* TFrameChain::GetFrame(int fileID, int frameID)
    {
        return this->GetFrame(this->GetGlobalID(fileID, frameID));
    }

    TFrame* TFrameChain::GetFrame(int globalID)
    {
        int fileID = this->GetFileID(globalID);
        if (fileID < 0) return NULL;

        int frameID = this->GetFrameID(globalID);
        if (frameID < 0) return NULL;
        
        return this->files[fileID][frameID];
    }

    std::vector<int> TFrameChain::Select(TString expr, std::vector<double> parameters)
    {
        std::vector<int> entryList;

        expr = expr.ReplaceAll("*", "true");
        TFormula *formula = expr.EqualTo("") ? NULL : new TFormula(ROOT::IOPlus::Helpers::GetRandomName("formula"), expr);
        if (formula == NULL) return entryList;

        if (!formula->IsValid()) {
            std::cerr << "Failed to compute formula `" << expr.Data() << "`" << std::endl;
            throw std::exception();
        }

        if (parameters.size()) {
            formula->SetParameters(&parameters[0]);
        }

        double duration = 0;
        for(int fileID = 0, N = this->GetNFiles(); fileID < N; fileID++) {

            for(int frameID = 0, M = this->GetNFrames(fileID); frameID < M; frameID++) {

                int globalID = this->GetGlobalID(fileID, frameID);

                int x = globalID;
                double y = this->GetGPS(fileID, frameID).first;

                duration += this->GetDuration(fileID, frameID);
                double z = duration;

                double t = 0;
                if((expr.Contains("x[3]") || expr.Contains("t")) && !expr.EqualTo("true")) {

                    TFrame *frame = this->GetFrame(fileID, frameID);
                    if(TPrint::WarningIf(frame == NULL, __METHOD_NAME__, (TString) "Failed to find frame #"+TString::Itoa(globalID,10)))
                        throw std::exception();

                    t = frame->GetDataQuality();
                }

                if(formula->Eval(x, y, z, t)) {
                    if(!expr.EqualTo("true")) TPrint::Debug(100, __METHOD_NAME__, (TString) "Frame #"+TString::Itoa(globalID,10)+" got a valid selection criteria.. " + expr);
                } else {
                    TPrint::Debug(100, __METHOD_NAME__, (TString) "Frame #"+TString::Itoa(globalID,10)+" doesn't meet selection criteria.. " + expr);
                    continue;
                }

                entryList.push_back(globalID);
            }
        }

        return entryList;
    }

    std::vector<int> TFrameChain::SelectByFrame(std::vector<int> frames)
    {
        int nFrames = this->GetNFrames();

        TString formula = "";
        for(int x = 0, N = frames.size(); x < N; x++) {

            if(frames[x] < 1 || frames[x] > nFrames)
                throw std::invalid_argument("Frame #"+TString::Itoa(frames[x],10)+" requested, but TFrameChain must range between 1 and "+TString::Itoa(nFrames, 10)+".");

            formula += (x > 0 ? " && " : "") + ("x == " + TString::Itoa(frames[x]-1,10));
        }

        return this->Select(formula);
    }

    std::vector<int> TFrameChain::SelectByFrame(int first, int last)
    {
        int nFrames = this->GetNFrames();
        if(first < 1 || first > nFrames)
            throw std::invalid_argument("Frame #"+TString::Itoa(first,10)+" requested, but TFrameChain must range between 1 and "+TString::Itoa(nFrames, 10)+".");
        if(last < 1 || last > nFrames)
            throw std::invalid_argument("Frame #"+TString::Itoa(last,10)+" requested, but TFrameChain must range between 1 and "+TString::Itoa(nFrames, 10)+".");

        TString formula = "";
        if(first >= 0) formula += TString("x >= "+TString::Itoa(first-1,10));
        if(last  >= 0) formula += (formula.EqualTo("") ? "" : " && ") + ("x <= "+TString::Itoa(last-1,10));

        return this->Select(formula);
    }

    std::vector<int> TFrameChain::SelectByGPS(std::vector<double> gpsList)
    {
        TString formula = "";
        for(int i = 0, N = gpsList.size(); i < N; i++)
            formula += (i > 0 ? " && " : "") + ("y == ["+TString::Itoa(i,10)+"]");

        return this->Select(formula, gpsList);
    }

    std::vector<int> TFrameChain::SelectByGPS(double gpsFirst, double gpsLast)
    {
        TString formula = "";
        if(gpsFirst >= 0) formula += TString(gpsFirst >= 0 ? "y >= [0]" : "");
        if(gpsLast  >= 0) formula += (formula.EqualTo("") ? "" : " && ") + TString(gpsLast  >= 0 ? "y <= [1]" : "");

        return this->Select(formula, std::vector<double>({gpsFirst, gpsLast}));
    }

    std::vector<int> TFrameChain::SelectByMaxDuration(double duration)
    {
        TString formula = (duration ? "z <= [0]" : "");
        return this->Select(formula, std::vector<double>({duration}));
    }

    std::vector<int> TFrameChain::SelectByMinDuration(double duration)
    {
        TString formula = (duration ? "z >= [0]" : "");
        return this->Select(formula, std::vector<double>({duration}));
    }

    std::vector<int> TFrameChain::SelectByDQ(unsigned int dataQuality)
    {
        TString formula = ("t == " + TString::UItoa(dataQuality, 10));
        return this->Select(formula);
    }

    GPS TFrameChain::GetGPS(int fileID, int frameID)
    {
        GPS gps = this->fileGPS[fileID < 0 ? 0 : fileID];
        if(frameID < 0) return gps;

        return this->GetFrame(fileID, frameID)->GetGPS();
    }

    double TFrameChain::GetDuration(int fileID, int frameID)
    {
        if(fileID < 0) {

            int totalDuration = 0;
            for(int i = 0, N = this->GetNFiles(); i < N; i++)
                totalDuration += this->GetDuration(i, frameID);

            return totalDuration;
        }

        if(fileID >= this->fileGPS.size() ) return 0;
        return frameID < 0 ? this->fileGPS[fileID].second-this->fileGPS[fileID].first : this->fileFrameLength[fileID];
    }

    TFrameChain *TFrameChain::Reshape(TString fName, int dt, Option_t *option, std::vector<int> globalIDs)
    {
        if(TPrint::ErrorIf(fName.EqualTo(""), __METHOD_NAME__, (TString) "No output file provided. Cannot save selected frames."))
            return NULL;

        if (!fName.EndsWith(".gwf")) {
            TPrint::Error(__METHOD_NAME__, (TString) "Wrong output extension provided. Please use \".gwf\" extension..");
            return NULL;
        }

        throw std::invalid_argument("Reshaping is not implemented yet.");

        // std::cout << this->fManager->Reshape() << std::endl;
        return NULL;//new TFrameChain(fName);
    }

    TFrameChain *TFrameChain::Trend(TString fName, int fs, Option_t *option, std::vector<int> globalIDs)
    {
        if(TPrint::ErrorIf(fName.EqualTo(""), __METHOD_NAME__, (TString) "No output file provided. Cannot save selected frames."))
            return NULL;

        if (!fName.EndsWith(".gwf")) {
            TPrint::Error(__METHOD_NAME__, (TString) "Wrong output extension provided. Please use \".gwf\" extension..");
            return NULL;
        }

        throw std::invalid_argument("Trending is not implemented yet.");

        // std::cout << this->fManager->Trend() << std::endl;
        return NULL;//new TFrameChain(fName);
    }

    int TFrameChain::Write(TString fName, Option_t *opt, int framesPerFile, std::vector<int> globalIDs)
    {
        if(TPrint::ErrorIf(fName.EqualTo(""), __METHOD_NAME__, (TString) "No output file provided. Cannot save selected frames."))
            return false;

        int nWritten = 0;
        if(globalIDs.size() < 1) return nWritten;

        if(framesPerFile < 0) framesPerFile = globalIDs.size();
        
        for(int b = 0, B = globalIDs.size()/framesPerFile; b < B; b++) {

            std::vector<int> _globalIDs = std::vector<int>(globalIDs.begin() + framesPerFile*b, globalIDs.begin() + framesPerFile*(b+1));
            if (fName.EndsWith(".gwf")) {
                nWritten += WriteGWF(fName, opt, _globalIDs);
                continue;
            }

            if (fName.EndsWith(".root")) {
                nWritten += WriteROOT(fName, opt, _globalIDs);
                continue;
            }

            TPrint::Error(__METHOD_NAME__, (TString) "Wrong output extension provided. Please use \".gwf\" or \".root\" extension..");
        }
        
        return false;
    }

    int TFrameChain::WriteGWF(TString fName, Option_t *opt, std::vector<int> globalIDs)
    {
        if(TPrint::ErrorIf(fName.EqualTo(""), __METHOD_NAME__, (TString) "No output file provided. Cannot save selected frames."))
            return false;

        if(TFileReader::HasPattern(fName)) {
            TPrint::Error(__METHOD_NAME__, "Output contains a pattern, this feature is not available for GWF files.. These already include GPS time and #frames.");
            return false;
        }
        
        int nWritten = 0;

        TString fDir = TFileReader::DirName(fName);
        TString fBasename = TFileReader::BaseName(fName, ".gwf");
        gSystem->mkdir(fDir, true);

        TString tmpDir = TPrint::CreateTemporaryPath();
        TString tmpOutput = tmpDir+"/"+fBasename+".gwf";
        gSystem->mkdir(tmpDir,true);

        TString option = opt;
                option.ToLower();

        bool bTree  = option.Contains("tree");
        bool bMore = option.Contains("more");
        int compression = TMath::Min(255, TMath::Max(0, this->compression));
        FrFile *oFile = FrFileONew(tmpOutput, compression);

        if(TPrint::ErrorIf(oFile == NULL, __METHOD_NAME__, (TString) "Cannot open output file \""+fName+"\""))
            return false;

        int lastFileID = -1;
        int timeReference = 0, length = 0;
        int nFrames = 0;
        
        // Remove duplicates
        std::sort(globalIDs.begin(), globalIDs.end());
        auto last = std::unique(globalIDs.begin(), globalIDs.end());
        globalIDs.erase(last, globalIDs.end());

        for(int i = 0, N = globalIDs.size() ? globalIDs.size() : this->GetNFrames(); i < N; i++)
        {
            int globalID = globalIDs.size() ? globalIDs[i] : i;
            int fileID = this->GetFileID(globalID);
            int frameID = this->GetFrameID(globalID);

            if(TPrint::WarningIf(fileID < 0 || frameID < 0, __METHOD_NAME__, (TString) "Failed to find global frame #"+TString::Itoa(globalID+1, 10)))
                continue;
            
            if(lastFileID < 0) lastFileID = fileID;

            TFrame* frame = this->GetFrame(globalID);
            if(TPrint::WarningIf(frame == NULL, __METHOD_NAME__, (TString) "Failed to find frame #"+TString::Itoa(frameID+1, 10)+" from file #"+TString::Itoa(fileID+1, 10)))
                continue;

            int ret = this->GetManager().Write(oFile, fileID, frameID, opt, frame->GetDataClass(), frame->GetDataTags());
            if(nWritten < 1) timeReference = frame->GetGPS().first;
            
            TString _fName = this->GetFileName(fileID);
            if(ret == FR_OK) {

                if(bMore) TPrint::Message(__METHOD_NAME__, (TString) "Frame #"+TString::Itoa(frameID+1, 10)+" from file #"+TString::Itoa(i+1, 10)+"/"+TString::Itoa(N, 10)+" ("+_fName+") saved.");
                else {
                
                    TString fNameWithBrackets = fName;
                            fNameWithBrackets.ReplaceAll(".gwf","-[..]-[..].gwf");

                    TPrint::ProgressBar((TString) "Saving frames from master #"+TString::Itoa(fileID+1, 10)+"/"+TString::Itoa(N, 10)+" into \""+fNameWithBrackets+"\"", "", frameID+1, N);
                }

                nWritten++;

            } else {

                TPrint::Warning(__METHOD_NAME__, (TString) "Failed to write frame #"+TString::Itoa(frameID+1, 10)+" from file #"+TString::Itoa(fileID+1, 10)+"/"+TString::Itoa(this->GetNFiles(), 10)+" ("+_fName+").");    
            }

            length += frame->GetDuration();
            nFrames++;
        }

        if(bMore) std::cout << std::endl;

        this->SetDebug(0);
        FrFileOEnd(oFile);
        this->SetDebug(this->GetVerbosity());

        if(bTree) { 
            fDir += "/" + TString::Itoa(timeReference,10)(0,6);
            gSystem->mkdir(fDir, true);
        }

        TString output = fDir+"/"+fBasename+"-"+TString::Itoa(timeReference,10)+"-"+TString::Itoa(length,10)+".gwf";        

        int ret = -1;
        if(nWritten > 0) {
            
            int ret = gSystem->Rename(tmpOutput, output.Data());
            if (ret < 0) TPrint::Error(__METHOD_NAME__, (TString) "Failed to save selected frames into `"+output.Data()+"` (do you have access right?).");
            else {
                
                TPrint::SetSingleCarriageReturn();
                TPrint::Message(__METHOD_NAME__, (TString) "Total segment length: %ds", length);
                TPrint::Message(__METHOD_NAME__, (TString) "%d frame(s) saved into `"+output.Data()+"`.", nFrames);
            }

            gSystem->Unlink(tmpDir);
        }

        return nWritten;
    }

    
    int TFrameChain::WriteROOT(TString fName, Option_t *opt, std::vector<int> globalIDs)
    {
        if(TPrint::ErrorIf(fName.EqualTo(""), __METHOD_NAME__, (TString) "No output file provided. Cannot save selected frames."))
            return false;

        int nWritten = 0;

        TFile *f = NULL;
        TString option = opt;
                option.ToLower();

        TList *outputList = new TList;
        TList *dirList = new TList;

        for(int k = 0, N = globalIDs.size() ? globalIDs.size() : this->GetNFrames(); k < N; k++)
        {
            int globalID = globalIDs.size() ? globalIDs[k] : k;
            if (globalIDs.size() && std::find(globalIDs.begin(), globalIDs.end(), globalID) == globalIDs.end()) continue;

            TFrame* frame = this->GetFrame(globalID);
            int fileID = this->GetFileID(globalID);
            int frameID = this->GetFrameID(globalID);

            if(TPrint::WarningIf(frame == NULL, __METHOD_NAME__, (TString) "Failed to find frame #"+TString::Itoa(frameID+1, 10)+" from file #"+TString::Itoa(fileID+1, 10)))
                continue;

            gPrint->ProgressBar(Form((TString) "Saving frame into `%s`.", fName.Data()), "noclean", k+1, N);
            std::vector<TFrameProxyT> proxyMap = frame->ReadProxy();

            TString _fName = fName;
            if(TFileReader::HasPattern(fName)) {

                std::vector<TString> identifiers;
                                     identifiers.push_back(TString::Itoa(frame->GetGPS().first,10));
                                     identifiers.push_back(TString::Itoa(frame->GetDuration(),10));

                _fName = TFileReader::SubstituteIdentifier(fName, identifiers);
            }

            TString _fopt = "update";
            if(option.Contains("recreate")) _fopt = "recreate";
            if(option.Contains("read_without_globalregistration")) _fopt = "read_without_globalregistration";
            if(option.Contains("new")) _fopt = "new";
            if(option.Contains("net")) _fopt = "net";
            if(option.Contains("web")) _fopt = "web";

                std::cout << f << std::endl;
            if(f == NULL || !_fName.EqualTo(f->GetName())) {

                if(f) {
                            
                    TIter Next(dirList);
                    TDirectory *structureDir = NULL;
                    while( (structureDir = (TDirectory*) Next()) )
                        nWritten += structureDir->Write();
    
                    dirList->Clear();
                    f->Close();
                }

                f = new TFile(_fName, _fopt);
            }
            
            bool bHisto = option.Contains("histo");
            for(int i = 0, I = proxyMap.size(); i < I; i++) {

                TFrameProxyAbstract *proxy = std::visit([](auto obj) -> TFrameProxyAbstract* { return (TFrameProxyAbstract*) obj; }, proxyMap[i]);
                for(int j = 0, J = proxy->GetNBaskets(); j < J; j++) {

                    TString name = proxy->GetName(j);
        
                    if(J > 512)
                        gPrint->ProgressBar(Form((TString) "Saving basket `%s`.", name.Data()), "", i+1, I);

                    TTree *tree = (TTree*) outputList->FindObject(name);
                    if(tree == NULL) {

                        TDirectory *dir = TFileReader::RecursiveMkdir(proxy->GetIdentifier(), f->GetDirectory("/"));
                        if(!dirList->FindObject(dir)) dirList->Add(dir);

                        outputList->Add(proxy->EmptyTree(name));
                        tree = (TTree*) outputList->FindObject(name);
                        if(tree == NULL) continue;

                        tree->SetDirectory(dir);
                    }

                    proxy->Hydrate(j, tree);
                }

                if(bHisto) {

                    for(int j = 0, J = outputList->GetEntries(); j < J; j++) {
                        
                        TTree *tree = (TTree*) outputList->At(j);
                        if(tree == NULL) continue;

                        TDirectory *dir = TFileReader::RecursiveMkdir(proxy->GetIdentifier(), f->GetDirectory("/"));
                        TH1 *hData = TFrameProxyAbstract::Histogram(tree, proxy->GetBranchName());
                             hData->SetDirectory(dir);
                    }
                }
            }

            TPrint::ClearLine();
        }

        if(f) {

            int _nWritten = 0;     
            TIter Next(dirList);
            TDirectory *structureDir = NULL;
            while( (structureDir = (TDirectory*) Next()) )
                _nWritten += structureDir->Write();

            if(_nWritten < 1) TPrint::Warning(__METHOD_NAME__, (TString) "No object were written into \""+fName.Data()+"\".");
            else TPrint::Message(__METHOD_NAME__, (TString) "Data saved into \""+fName.Data()+"\".");
            nWritten += _nWritten;

            std::cout << std::endl;
            dirList->Clear();
            f->Close();
        }

        delete outputList;
        delete dirList;

        return nWritten;
    }

    TObject* TFrameChain::GetObject(TString objName, std::vector<int> globalIDs)
    {
        TTree *tree = NULL;

        for(int i = 0, N = globalIDs.size() ? globalIDs.size() : this->GetNFrames(); i < N; i++)
        {
            int globalID = globalIDs.size() ? globalIDs[i] : i;
            if (globalIDs.size() && std::find(globalIDs.begin(), globalIDs.end(), globalID) == globalIDs.end()) continue;

            TFrame* frame = this->GetFrame(globalID);
            int fileID = this->GetFileID(globalID);
            int frameID = this->GetFrameID(globalID);

            if(TPrint::WarningIf(frame == NULL, __METHOD_NAME__, (TString) "Failed to find frame #"+TString::Itoa(frameID+1, 10)+" from file #"+TString::Itoa(fileID+1, 10)))
                continue;

            std::vector<TFrameProxyT> proxyMap = frame->ReadProxy();
            for(int i = 0, I = proxyMap.size(); i < I; i++) {

                TFrameProxyAbstract *proxy = std::visit([](auto obj) -> TFrameProxyAbstract* { return (TFrameProxyAbstract*) obj; }, proxyMap[i]);
                for(int j = 0, J = proxy->GetNBaskets(); j < J; j++) {

                    TString _objName = proxy->GetName(j);
                    TString _objTitle = proxy->GetMessage(j);
                    if(!objName.EqualTo(_objName)) continue;
                    
                    if (tree == NULL)
                        tree = proxy->EmptyTree(_objName, _objTitle);

                    proxy->Hydrate(j, tree);
                }
            }
        }

        return (TObject*) tree;
    }

    TObject *TFrameChain::GetObject(TString objName, TString branchName, Option_t *option, std::vector<int> globalIDs)
    {
        TTree *tree = (TTree *) this->GetObject(objName, globalIDs);
        if(tree == NULL) return NULL;
            
        TH1 *hData = TFrameProxyAbstract::Histogram(tree, branchName, option);
        tree->Delete();

        return (TObject *) hData;
    }

};
