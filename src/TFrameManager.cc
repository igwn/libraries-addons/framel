/**
 **********************************************
 *
 * \file TFrameManager.cc
 * \brief Source code of the TFrameManager class
 * \author Marco Meyer \<marco.meyer@cern.ch\>
 *
 *********************************************
 */

#include "FrameFormat/TFrameManager.h"
ClassImp(FrameFormat::TFrameManager)

#include "FrameFormat/Impl/TFrameHeader.h"
#include "FrameL.h"

#include <algorithm>
#include <vector>

namespace FrameFormat {

    TFrameManager::TFrameManager(FrFile &fMaster, std::vector<int> frameAllocation): fMaster(fMaster)
    {
        TString fName = ROOT::IOPlus::Helpers::Implode(" ", this->GetFileNames(&fMaster));

        // Main reading head declaration
        this->fHead.push_back(new TFrameRHead(0, FrFileINew(fName))); 

        if( ::ROOT::IsImplicitMTEnabled() ) { // Additional reading heads are declared if implicit MT enabled

            TPrint::Debug(10, __METHOD_NAME__, (TString) "Frame library using parallel reading: %d core(s)", ::ROOT::GetThreadPoolSize());
            for(int i = 0; i < ::ROOT::GetThreadPoolSize(); i++) {
                this->fHead.push_back(new TFrameRHead(i+1, FrFileINew(fName)));
            }
        }

        this->frame[&fMaster] = std::vector<std::vector<FrameH*>>(frameAllocation.size(), std::vector<FrameH*>({}));
        for(int i = 0; i < frameAllocation.size(); i++) {
            this->frame[&fMaster][i] = std::vector<FrameH*>(frameAllocation[i], NULL);
        }

        for(int h = 0; h < this->fHead.size(); h++) {

            this->frame[this->fHead[h]->GetFile()] = std::vector<std::vector<FrameH*>>(frameAllocation.size(), std::vector<FrameH*>({}));
            for(int i = 0; i < frameAllocation.size(); i++) {
                this->frame[this->fHead[h]->GetFile()][i] = std::vector<FrameH*>(frameAllocation[i], NULL);
            }
        }
    }

    TFrameManager::~TFrameManager()
    {
        for(int i = 0, N = fHead.size(); i < N; i++) {

            if (fHead[i] != NULL) {

                FrFileIEnd(fHead[i]->GetFile());
                fHead[i] = NULL;
            }
        }
    }

    FrFile &TFrameManager::GetMaster(int fileID, int frameID)
    {
        this->Seek(&this->fMaster, fileID, frameID);
        
        return this->fMaster;
    }

    TFrameRHead *TFrameManager::GetRHead(int headID)
    {
        int poolSize = TMath::Max(1, (int) ::ROOT::GetThreadPoolSize());
        int modulo = TMath::Min((int) this->fHead.size(), poolSize);

        headID = headID - modulo * TMath::Floor(headID / modulo);
        TFrameRHead *head = NULL;
        for(int i = 0, N = this->fHead.size(); i < N; i++) {

            if (this->fHead[i]->GetID() == headID) {
                head = this->fHead[i];
                break;
            }
        }

        return head;
    }

    FrFile *TFrameManager::GetFile(TFrameRHead &head, int fileID, int frameID)
    {
        FrFile *fHead = head.GetFile();
        return this->Seek(fHead, fileID, frameID) ? fHead : NULL;
    }

    std::vector<TString> TFrameManager::GetFileNames(FrFile *file)
    {
        std::vector<TString> fileNames;
        if(file == NULL) return fileNames;

        FrFileH *fileHeader = file->fileH;
        do { fileNames.push_back(fileHeader->fileName); }
        while ((fileHeader = fileHeader->next));

        return fileNames;
    }

    int TFrameManager::GetFileID(FrFile *file, FrFileH *fileHeader)
    {
        if(file == NULL) return -1;

        fileHeader = fileHeader ? fileHeader : file->current;
        if(fileHeader == NULL) return -1;

        FrFileH *_fileHeader = file->fileH;
        for(int i = 0; _fileHeader != NULL; i++) {

            if(_fileHeader == fileHeader) return i;
            _fileHeader = _fileHeader->next;
        }

        return -1;
    }

    int TFrameManager::GetFrameID(FrFile *file, FrameH *frameHeader) // FrameHeader search could be implemented (if needed)
    {
        if(frameHeader != NULL) 
            throw std::invalid_argument("Finding FrameH in FrFile is not implemented yet.");

        if(file == NULL) return -1;

        FrTOC *toc = FrTOCReadFull(file);
        if(file) {

            int tellp = FrFileIOTell(file);
            
            for(int frameID = 0; frameID < toc->nFrame; frameID++) {

                if(tellp == toc->positionH[frameID])
                    return frameID;
            }
        }

        return -1;
    }

    int TFrameManager::GetNFiles(FrFile *file)
    {
        int nFiles = 0;
        if(file == NULL) return nFiles;

        FrFileH *_fileHeader = file->fileH;
        for(int i = 0; _fileHeader != NULL; i++) {
            _fileHeader = _fileHeader->next;
            nFiles++;
        }

        return nFiles;
    }

    int TFrameManager::GetNFrames(FrFile *file, int fileID)
    {
        int nFrames = 0;
        if(file == NULL) return nFrames;

        FrFileH *_fileHeader = file->fileH;
        for(int i = 0; _fileHeader != NULL; i++) {

            if(fileID == i || fileID < 0) {

                FrTOC *toc = this->ReadTOC(file, fileID);
                if(toc == NULL) {
                
                    TPrint::Warning(__METHOD_NAME__, Form("Failed to read TOC file #%d", fileID+1));
                    continue; // Failed to load file.. @TODO an alternative loading procedure if TOC not found
                }

                nFrames += toc->nFrame;
            }

            if(fileID == i) break;
            _fileHeader = _fileHeader->next;
        }

        return nFrames;
    }

    bool TFrameManager::Rewind(FrFile *f, int fileID)
    {
        if(fileID < 0) this->Seek(f, fileID);

        int i = this->Tell(f, EIndexing::kFile);
        TPrint::Debug(100, __METHOD_NAME__, (TString) "Rewind to file #%d", i);
        return this->Seek(f, i, 0);
    }

    bool TFrameManager::PreviousFile(FrFile *f)
    {
        int i = this->Tell(f, EIndexing::kFile);
        return this->Seek(f, i-1, 0);
    }

    bool TFrameManager::Previous(FrFile *f)
    {
        int i = this->Tell(f, EIndexing::kFile);
        int j = this->Tell(f, EIndexing::kFrame);
        return this->Seek(f, i-1);
    }

    bool TFrameManager::Next(FrFile *f)
    {
        int i = this->Tell(f, EIndexing::kFile);
        int j = this->Tell(f, EIndexing::kFrame);
        return this->Seek(f, i, j+1);
    }

    bool TFrameManager::NextFile(FrFile *f)
    {
        int i = this->Tell(f, EIndexing::kFile);
        return this->Seek(f, i+1, 0);
    }

    GPS TFrameManager::GetGPS(FrFile *file)
    {
        if(file == NULL) return std::make_pair(0,0);
        return std::make_pair(file->startTime, file->closingTime);
    }

    GPS TFrameManager::GetGPS(FrFile *f, int fileID, int frameID)
    {
        GPS gps = std::make_pair(0,0);
        if(f == NULL) return gps;

        if (fileID < 0) {
            fileID = this->GetFileID(f);
            frameID = frameID < 0 ? 0 : frameID;
        }

        if (frameID < 0) {
            frameID = this->GetFrameID(f);
        }
        
        FrTOC *toc = this->ReadTOC(f, fileID);
        if(toc == NULL) {
        
            TPrint::Warning(__METHOD_NAME__, Form("Failed to read TOC file #%d", fileID+1));
            return gps;
        }

        gps.first = gClockGPS->Timestamp(toc->GTimeS[frameID], toc->GTimeN[frameID]);
        gps.second = gps.first + toc->dt[frameID];

        return gps;
    }

    double TFrameManager::GetDuration(FrFile *file)
    {
        if(file == NULL) return 0;
        return file->closingTime - file->startTime;
    }

    double TFrameManager::GetDuration(FrFile *f, int fileID, int frameID)
    {
        if(f == NULL) return 0;

        if (fileID < 0) {
            fileID = this->GetFileID(f);
            frameID = frameID < 0 ? 0 : frameID;
        }
        
        if (frameID < 0) {
            frameID = this->GetFrameID(f);
        }
        
        FrTOC *toc = this->ReadTOC(f, fileID);
        if(toc == NULL) {
        
            TPrint::Warning(__METHOD_NAME__, Form("Failed to read TOC file #%d", fileID+1));
            return 0;
        }

        return toc->dt[frameID];
    }

    unsigned int TFrameManager::GetDataQuality  (FrFile *f, int fileID, int frameID)
    {
        if(f == NULL) return 0;

        if (fileID < 0) {
            fileID = this->GetFileID(f);
            frameID = frameID < 0 ? 0 : frameID;
        }
        
        if (frameID < 0) {
            frameID = this->GetFrameID(f);
        }
        
        FrTOC *toc = this->ReadTOC(f, fileID);
        if(toc == NULL) {
        
            TPrint::Warning(__METHOD_NAME__, Form("Failed to read TOC file #%d", fileID+1));
            return 0;
        }

        return toc->dataQuality[frameID];
    }

    bool TFrameManager::Seek(FrFile *f, int fileID, int frameID)
    {
        if(f == NULL) return false;

        TFrameManager::SetDebug(0);

        if(TPrint::IsDebugEnabled(100)) TPrint::SetSingleCarriageReturn();

        if(fileID == this->GetFileID(f) && frameID == this->GetFrameID(f)) {

            TFrameManager::SetDebug(this->GetVerbosity());
            TPrint::Debug(100, __METHOD_NAME__, (TString) "Pointer in place, no seek required (fileID, frameID) = (%d, %d)", fileID, frameID);
            return true;
        }

        TPrint::Debug(100, __METHOD_NAME__, (TString) "Seek for (fileID, frameID) = (%d, %d) << currently at (%d, %d)", fileID, frameID, this->GetFileID(f), this->GetFrameID(f));
        if(fileID < 0) fileID = this->GetFileID(f); // Select current file

        if(this->GetFrameID(f) < 0 || this->GetFileID(f) > fileID || this->GetFrameID(f) > frameID) {
            TPrint::Debug(100, __METHOD_NAME__, (TString) "Rewind to file #%d at frame %d << currently at (%d, %d)", fileID+1, frameID+1, this->GetFileID(f), this->GetFrameID(f));
            FrFileIRewind(f); // Rewind at the beginning of the file..
        }

        int _fileID = fileID - this->GetFileID(f);
        while(_fileID-- > 0) {

            if(f->current == NULL) {
                TFrameManager::SetDebug(this->GetVerbosity());
                return false;
            }
            if(f->current->next == NULL) {
                TFrameManager::SetDebug(this->GetVerbosity());
                return false;
            }

            if(FrFileINext(f, 0., 0., NULL, FR_NO) != 0) {

                TPrint::Warning(__METHOD_NAME__, (TString) "Failed to seek header file #"+TString::Itoa(fileID,10) + ": " + TPrint::MakeItShorter(f->current->fileName));    
                TFrameManager::SetDebug(this->GetVerbosity());
                return false;
            }
        }

        if(frameID > 0) {

            FrTOC *toc = FrTOCReadFull(f);

            if(frameID >= toc->nFrame) {
                TFrameManager::SetDebug(this->GetVerbosity());
                TPrint::Debug(100, __METHOD_NAME__, (TString) "Seek next file (fileID, frameID) = (%d, %d) << currently at (%d, %d)", fileID+1, frameID - toc->nFrame, this->GetFileID(f), this->GetFrameID(f));
                return this->Seek(f, fileID+1, frameID - toc->nFrame);
            }

            if(toc != NULL) {

                if(frameID != this->GetFrameID(f)) {

                    if(FrFileIOSet(f, toc->positionH[frameID]) == -1) {
                        TPrint::Error(__METHOD_NAME__, (TString) "Reading table of content.. failed to seek to (fileID, frameID) = (%d, %d)", fileID, frameID);
                    } else {
                        TPrint::Debug(100, __METHOD_NAME__, (TString) "Reading table of content.. going straight to frame #%d", frameID);
                    }
                }

            } else { // When no TOC..... no so good to do, but ok..

                if(this->GetFrameID(f) >= 0)
                    frameID -= this->GetFrameID(f);

                TPrint::Debug(10, __METHOD_NAME__, (TString) "No TOC found.. Reading frame one by one..");
                while(frameID-- > 0) {

                    TPrint::Debug(10, __METHOD_NAME__, (TString) "Reading frame #%d", frameID+1);
                    FrameReadT(f, this->GetGPS(f).first, " ");
                }
            }
        }

        TFrameManager::SetDebug(this->GetVerbosity());
        return true;
    }

    int TFrameManager::Tell(FrFile *file, EIndexing repr)
    {
        switch(repr)
        {
            case EIndexing::kFile:
                return this->GetFileID(file);
            break;

            case EIndexing::kFrame:
                return this->GetFrameID(file);
            break;
        }

        return 0;
    }

    FrameH* TFrameManager::ReadHeader(FrFile *file, int fileID)
    {
        if(!this->Seek(file, fileID)) return NULL;

        this->SetDebug(0);
        FrameH *header = FrameHRead(file);
        this->SetDebug(this->GetVerbosity());

        return header;
    }

    FrTOC* TFrameManager::ReadTOC(FrFile *file, int fileID)
    {
        if(!this->Seek(file, fileID, 0)) return NULL;

        this->SetDebug(0);
        FrTOC *toc = FrTOCReadFull(file);
        this->SetDebug(this->GetVerbosity());

        return toc;
    }

    void TFrameManager::DumpTOC(FrFile *file, int fileID, std::vector<TString> tags)
    {
        return DumpTOC(file, fileID, ROOT::IOPlus::Helpers::Implode(" ", tags).Data());
    }

    void TFrameManager::DumpTOC(FrFile *file, int fileID, const char *tags)
    { 
        FrTOC *toc = this->ReadTOC(fileID);
        FrTOCDump(toc, stdout, this->GetVerbosity(), (char*) tags);
    }

    FrameH *TFrameManager::Read(FrFile *file, int fileID, int frameID, std::vector<TString> tags) 
    { 
        return Read(file, fileID, frameID, ROOT::IOPlus::Helpers::Implode(" ", tags).Data());
    }

    TString TFrameManager::GetUniqueID(int fileID, int frameID)
    {
        if(frameID < 0) return TString::Itoa(fileID,10);
        return TString::Itoa(fileID,10)+"-"+TString::Itoa(frameID,10);
    }

    TString TFrameManager::GetUniqueID(TFrameRHead &head, int fileID, int frameID)
    {
        return this->GetUniqueID(fileID, frameID)+"@"+TString::Itoa(head.GetID(),10);
    }

    FrameH *TFrameManager::Read(FrFile *file, int fileID, int frameID, const char *tags)
    {
        if(!this->Seek(file, fileID, frameID)) return NULL;
        if(!this->Find(file, fileID, frameID)) {

            this->SetDebug(0);
            frame[file][fileID][frameID] = FrameReadT(file, this->GetGPS(file, fileID, frameID).first, tags);
            this->SetDebug(this->GetVerbosity());
        }

        return frame[file][fileID][frameID];
    }

    FrEndOfFile* TFrameManager::ReadEOF(FrFile *file, int fileID)
    {
        if(!this->Rewind(file, fileID)) return NULL;
        return FrEndOfFileReadAndReturn(file);
    }

    bool TFrameManager::Find(FrFile *file, int fileID, int frameID)
    {
        if (fileID < 0) {
            fileID = this->GetFileID(file);
            frameID = frameID < 0 ? 0 : frameID;
        }
        
        if (frameID < 0) {
            frameID = this->GetFrameID(file);
        }

        return frame[file][fileID][frameID] != NULL;
    }

    void TFrameManager::Free(FrFile *file, int fileID, int frameID) { 

        if(frame[file][fileID][frameID] == NULL) return;
        
        for(int i = 0, I = this->GetNFiles(file); i < I; i++) {

            if(fileID != i && fileID >= 0) continue;
            for(int j = 0, J = this->GetNFrames(file, fileID); j < J; j++) {

                if(frameID != j && frameID >= 0) continue;
                if(frame[file][i][j] != NULL) {

                        FrameFree(frame[file][fileID][frameID]);
                        frame[file][fileID][frameID] = NULL;
                }
            }
        }
    }

    int TFrameManager::Write(FrFile *oFile, int fileID, int frameID, Option_t *option, std::vector<FrType> dataTypeList, std::vector<TString> tags)
    {
        return Write(oFile, fileID, frameID, option, dataTypeList, ROOT::IOPlus::Helpers::Implode(" ", tags).Data());
    }
    
    int TFrameManager::Write(FrFile *oFile, int fileID, int frameID, Option_t *option, std::vector<FrType> dataTypeList, const char* tags)
    {
        if(TPrint::ErrorIf(oFile == NULL, __METHOD_NAME__, (TString) "No output file provided. Cannot save the selected frame."))
            return FR_ERROR_NO_FILE;

        FrameH *frame = this->Read(&this->GetMaster(), fileID, frameID);
        if(frame == NULL) return FR_ERROR_NO_FRAME;

        TString opt = option;
                opt.ToLower();

        if(dataTypeList.size() == 0) FrameTag(frame, const_cast<char *>(tags));
        else {

            for(int i = 0, N = dataTypeList.size(); i < N; i++) {

                FrType dataType = dataTypeList[i];
                switch(dataType)
                {
                    case FrType::FrMsg     : 
                        FrameTagMsg(frame, const_cast<char *>(tags));
                        break;
                    case FrType::FrProcData: 
                        FrameTagProc(frame, const_cast<char *>(tags));
                        break;
                    case FrType::FrSimData : 
                        FrameTagSim(frame, const_cast<char *>(tags));
                        break;
                    case FrType::FrEvent   : 
                        FrameTagEvent(frame, const_cast<char *>(tags));
                        break;
                    case FrType::FrSimEvent: 
                        FrameTagSimEvt(frame, const_cast<char *>(tags));
                        break;
                    case FrType::FrSummary : 
                        FrameTagSum(frame, const_cast<char *>(tags));
                        break;
                    case FrType::FrAdcData : 
                        FrameTagAdc(frame, const_cast<char *>(tags));
                        break;
                    case FrType::FrSerData : 
                        FrameTagSer(frame, const_cast<char *>(tags));
                        break;
                    case FrType::FrStatData : 
                        FrameTagStat(frame, const_cast<char *>(tags));
                        break;

                    default:
                        break;
                }
            }
        }

        this->SetDebug(0);
        int ret = FrameWrite(frame, oFile);
        this->SetDebug(this->GetVerbosity());
    
        FrameFree(frame);
        frame = NULL;
    
        return ret;
    }

    int TFrameManager::Trend(FrFile *oFile, std::vector<FrAdcData*> adc, int fs, Option_t *option)
    {
        int nWritten = 0;
        TString opt = option;
                opt.ToLower();

        bool bUpdate = opt.Contains("update");
        if(adc.size() < 1) return nWritten;

        std::cout << "TREND TODO: " << adc.size() << std::endl;
        return nWritten;
    }

    FrameH* TFrameManager::Reshape(FrameH* frame, int frameLength, Option_t *option)
    {
        return Reshape(NULL, frame, frameLength, option);
    }

    FrameH* TFrameManager::Reshape(FrameH* frame1, FrameH* frame2, int frameLength, Option_t *option)
    {
        if(frame2 == NULL) return frame1;

        // Reshape
        std::cout << "RESHAPE TODO" << std::endl;
        return NULL;
    }
};
