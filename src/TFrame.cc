/**
 **********************************************
 *
 * \file TFrame.cc
 * \brief Source code of the TFrame class
 * \author Marco Meyer \<marco.meyer@cern.ch\>
 *
 *********************************************
 */

#include "FrameFormat/TFrame.h"
ClassImp(FrameFormat::TFrame)

#include "FrameFormat/Impl/TFrameHeader.h"
#include "FrameFormat/TFrameProxy.h"

namespace FrameFormat {

        TFrame::TFrame(TFrameChain *fChain, int fileID, int frameID, TString tags, std::vector<FrType> dataClass, int decimate, std::vector<int> channelGroups, std::vector<int> channelNumbers) 
              : fChain(fChain), fileID(fileID), frameID(frameID), dataClass(dataClass), tags(tags), decimate(decimate), channelGroups(channelGroups), channelNumbers(channelNumbers)
        {
                this->tags.Strip();

                TString _tags = this->tags;
                if(_tags.EqualTo("")) _tags = "*";

                std::vector<TString> _tagList = ROOT::IOPlus::Helpers::Explode(" ", _tags);
                for(int i = 0, N = _tagList.size(); i < N; i++)
                { 
                        TString _tag = _tagList[i].ReplaceAll("*", ".*");
                        if(_tagList[i].BeginsWith("-")) this->tagAntiRegex.push_back(TPRegexp("^"+_tag(1, _tag.Length()-1)+"$"));
                        else this->tagRegex.push_back(TPRegexp("^"+_tag+"$"));
                }
        }

        TFrame::~TFrame() { }

        TString TFrame::GetUniqueID()
        {
                return TString::Itoa(fileID,10)+"-"+TString::Itoa(frameID, 10);
        }

        bool TFrame::IsValidDataClass(FrType _dataClass)
        {
                return dataClass.size() == 0 || std::find(dataClass.begin(), dataClass.end(), _dataClass) != dataClass.end();
        }

        bool TFrame::IsValidTag(TString name)
        {
                bool validTag = !this->tagRegex.size();
                for(int i = 0, N = this->tagRegex.size(); i < N; i++) {

                        if(name.Contains(this->tagRegex[i])) {
                                validTag = true;
                                break;
                        }
                }

                for(int i = 0, N = this->tagAntiRegex.size(); i < N; i++) {

                        if(name.Contains(this->tagAntiRegex[i])) {
                                validTag = false;
                                break;
                        }
                }

                return validTag;
        }

        bool TFrame::Load() {
                return ( &this->Read() ) != NULL;
        }

        TFrameRHead &TFrame::GetRHead() 
        {
                int globalID = this->fChain->GetGlobalID(this);
                return *this->GetManager().GetRHead(globalID);
        }

        bool TFrame::AddFriend(TFrame *frame)
        {
                if ( std::find(fSiblings.begin(), fSiblings.end(), frame) == fSiblings.end() ) {
               
                        gPrint->Debug(10, __METHOD_NAME__, "Adding new sibling frame from %s", frame->GetFileName().Data());
                        fSiblings.push_back(frame);
                        return true;
                }

                return false;
        }

        void TFrame::RemoveFriend(TFrame *frame)
        {
                auto id = std::find(fSiblings.begin(), fSiblings.end(), frame);
                if ( id != fSiblings.end()) {

                        fSiblings.erase(id);
                }
        }

        std::vector<TString> TFrame::GetAdcNames()
        {
                std::vector<TString> names;
                FrTOC *toc = this->GetManager().ReadTOC(&this->GetRHead(), fileID);
                if(toc == NULL) return {};

                FrTOCts *ts;
                for(ts = toc->adc; ts != NULL; ts = ts->next) {

                        if(this->IsValidTag(ts->name)) 
                        names.push_back(ts->name);
                }

                return names;
        }

        std::vector<TString> TFrame::GetProcNames()
        {
                std::vector<TString> names;
                FrTOC *toc = this->GetManager().ReadTOC(&this->GetRHead(), fileID);
                if(toc == NULL) return {};

                FrTOCts *ts;
                for(ts = toc->proc; ts != NULL; ts = ts->next) {

                        if(this->IsValidTag(ts->name)) 
                        names.push_back(ts->name);
                }

                return names;
        }

        std::vector<TString> TFrame::GetSimNames() 
        {
                std::vector<TString> names;
                FrTOC *toc = this->GetManager().ReadTOC(&this->GetRHead(), fileID);
                if(toc == NULL) return {};

                FrTOCts *ts;
                for(ts = toc->sim; ts != NULL; ts = ts->next) {

                        if(this->IsValidTag(ts->name)) 
                        names.push_back(ts->name);
                }

                return names;
        }

        std::vector<TString> TFrame::GetSerNames()
        {
                std::vector<TString> names;
                FrTOC *toc = this->GetManager().ReadTOC(&this->GetRHead(), fileID);
                if(toc == NULL) return {};

                FrTOCts *ts;
                for(ts = toc->ser; ts != NULL; ts = ts->next) {

                        if(this->IsValidTag(ts->name)) 
                        names.push_back(ts->name);
                }

                return names;
        }

        std::vector<TString> TFrame::GetSummaryNames()
        {
                std::vector<TString> names;
                FrTOC *toc = this->GetManager().ReadTOC(&this->GetRHead(), fileID);
                if(toc == NULL) return {};
                
                FrTOCts *ts;
                for(ts = toc->summary; ts != NULL; ts = ts->next) {
                        
                        if(this->IsValidTag(ts->name)) 
                        names.push_back(ts->name);
                }

                return names;
        }

        TFrame *TFrame::Prev()
        {
                int globalID = this->fChain->GetGlobalID(this);
                if(globalID <= 0) return NULL;

                return this->fChain->GetFrame(globalID-1);
        }
        
        TFrame *TFrame::Next()
        {
                int globalID = this->fChain->GetGlobalID(this);
                if(globalID >= this->fChain->GetNFrames()-1) return NULL;

                return this->fChain->GetFrame(globalID+1);
        }
        
        bool TFrame::IsContiguous()
        {
                TFrame *prev = this->Prev();
                if(prev && !ROOT::IOPlus::Helpers::EpsilonEqualTo(prev->GetGPS().second, this->GetGPS().first))
                        return false;

                TFrame *next = this->Next();
                if(next && !ROOT::IOPlus::Helpers::EpsilonEqualTo(next->GetGPS().first, this->GetGPS().second))
                        return false;

                return true;
        }
        
        TFrame &TFrame::Read()
        {
                this->GetManager().Seek(&this->GetRHead(), fileID, frameID);

                if(this->proxyMap.size() == 0) {

                        FrameH *frame = this->GetManager().Read(&this->GetRHead(), fileID, frameID, this->tags);
                        if(frame == NULL) return *this;

                        if(frame->type != nullptr && IsValidDataClass(FrType::FrVect))
                                this->proxyMap.push_back(new TFrameProxy<TFrVect>(this, "type", frame->type));
                        if(frame->user != nullptr && IsValidDataClass(FrType::FrVect))
                                this->proxyMap.push_back(new TFrameProxy<TFrVect>(this, "user", frame->user));
                        if(frame->detectSim != nullptr && IsValidDataClass(FrType::FrDetector))
                                this->proxyMap.push_back(new TFrameProxy<TFrDetector>(this, "detectSim", frame->detectSim));
                        if(frame->detectProc != nullptr && IsValidDataClass(FrType::FrDetector))
                                this->proxyMap.push_back(new TFrameProxy<TFrDetector>(this, "detectProc", frame->detectProc));
                        if(frame->history != nullptr && IsValidDataClass(FrType::FrHistory))
                                this->proxyMap.push_back(new TFrameProxy<TFrHistory>(this, "history", frame->history));

                        if(frame->rawData != nullptr) {

                                if(frame->rawData != nullptr && IsValidDataClass(FrType::FrRawData))
                                        this->proxyMap.push_back(new TFrameProxy<TFrRawData>(this, "rawData/", frame->rawData));        
                                if(frame->rawData->firstSer != nullptr && IsValidDataClass(FrType::FrSerData))
                                        this->proxyMap.push_back(new TFrameProxy<TFrSerData>(this, "rawData/serData", frame->rawData->firstSer));
                                if(frame->rawData->firstAdc != nullptr && IsValidDataClass(FrType::FrAdcData))
                                        this->proxyMap.push_back(new TFrameProxy<TFrAdcData>(this, "rawData/adcData", frame->rawData->firstAdc));
                                if(frame->rawData->firstTable != nullptr && IsValidDataClass(FrType::FrTable))
                                        this->proxyMap.push_back(new TFrameProxy<TFrTable>(this, "rawData/table", frame->rawData->firstTable));
                                if(frame->rawData->logMsg != nullptr && IsValidDataClass(FrType::FrMsg))
                                        this->proxyMap.push_back(new TFrameProxy<TFrMsg>(this, "rawData/msg", frame->rawData->logMsg));
                        }

                        if(frame->procData != nullptr && IsValidDataClass(FrType::FrProcData))
                                this->proxyMap.push_back(new TFrameProxy<TFrProcData>(this, "procData", frame->procData));
                        if(frame->simData != nullptr && IsValidDataClass(FrType::FrSimData))
                                this->proxyMap.push_back(new TFrameProxy<TFrSimData>(this, "simData", frame->simData));
                        if(frame->event != nullptr && IsValidDataClass(FrType::FrEvent))
                                this->proxyMap.push_back(new TFrameProxy<TFrEvent>(this, "event", frame->event));
                        if(frame->simEvent != nullptr && IsValidDataClass(FrType::FrSimEvent))
                                this->proxyMap.push_back(new TFrameProxy<TFrSimEvent>(this, "simEvent", frame->simEvent));
                        if(frame->summaryData != nullptr && IsValidDataClass(FrType::FrSummary))
                                this->proxyMap.push_back(new TFrameProxy<TFrSummary>(this, "summaryData", frame->summaryData));

                        if(frame->auxData != nullptr && IsValidDataClass(FrType::FrVect))
                                this->proxyMap.push_back(new TFrameProxy<TFrVect>(this, "auxData", frame->auxData));
                        if(frame->auxTable != nullptr && IsValidDataClass(FrType::FrTable))
                                this->proxyMap.push_back(new TFrameProxy<TFrTable>(this, "auxTable", frame->auxTable));

                        // Old standard
                        // if(frame->procData != nullptr IsValidDataClass(FrType::FrProcData))
                        //     this->proxyMap.push_back(new TFrameProxy(this, "procDataOld", FrType::FrProcData, frame->procDataOld));
                        // if(frame->simData != nullptr IsValidDataClass(FrType::FrSimData))
                        //     this->proxyMap.push_back(new TFrameProxy(this, "simDataOld", FrType::FrSimData, frame->simDataOld));
                        // if(frame->event != nullptr IsValidDataClass(FrType::FrEvent))
                        //     this->proxyMap.push_back(new TFrameProxy(this, "eventOld", FrType::FrEvent, frame->eventOld));
                        // if(frame->simEvent != nullptr IsValidDataClass(FrType::FrSimEvent))
                        //     this->proxyMap.push_back(new TFrameProxy(this, "simEventOld", FrType::FrSimEvent, frame->simEventOld));
                        // if(frame->summaryData != nullptr IsValidDataClass(FrType::FrSummary))
                        //     this->proxyMap.push_back(new TFrameProxy(this, "summaryDataOld", FrType::FrSummary, frame->summaryDataOld));
                }

                return *this;
        }

        std::vector<FrType> TFrame::ReadTypes()
        {
                this->Read();
                
                std::vector<FrType> typeList;
                for(int i = 0, I = this->GetNProxies(); i < I; i++) {

                        TString type = std::visit([](auto* obj) -> TString { return obj->GetType(); }, this->proxyMap[i]);

                        if(type.EqualTo("FrameFormat::TFrAdcData"))
                                typeList.push_back(FrameFormat::FrType::FrAdcData);
                        if(type.EqualTo("FrameFormat::TFrDetector"))
                                typeList.push_back(FrameFormat::FrType::FrDetector);
                        if(type.EqualTo("FrameFormat::TFrEvent"))
                                typeList.push_back(FrameFormat::FrType::FrEvent);
                        if(type.EqualTo("FrameFormat::TFrHistory"))
                                typeList.push_back(FrameFormat::FrType::FrHistory);
                        if(type.EqualTo("FrameFormat::TFrMsg"))
                                typeList.push_back(FrameFormat::FrType::FrMsg);
                        if(type.EqualTo("FrameFormat::TFrProcData"))
                                typeList.push_back(FrameFormat::FrType::FrProcData);
                        if(type.EqualTo("FrameFormat::TFrRawData"))
                                typeList.push_back(FrameFormat::FrType::FrRawData);
                        if(type.EqualTo("FrameFormat::TFrSerData"))
                                typeList.push_back(FrameFormat::FrType::FrSerData);
                        if(type.EqualTo("FrameFormat::TFrSimData"))
                                typeList.push_back(FrameFormat::FrType::FrSimData);
                        if(type.EqualTo("FrameFormat::TFrSimEvent"))
                                typeList.push_back(FrameFormat::FrType::FrSimEvent);
                        if(type.EqualTo("FrameFormat::TFrStatData"))
                                typeList.push_back(FrameFormat::FrType::FrStatData);
                        if(type.EqualTo("FrameFormat::TFrSummary"))
                                typeList.push_back(FrameFormat::FrType::FrSummary);
                        if(type.EqualTo("FrameFormat::TFrTable"))
                                typeList.push_back(FrameFormat::FrType::FrTable);
                        if(type.EqualTo("FrameFormat::TFrVect"))
                                typeList.push_back(FrameFormat::FrType::FrVect);
                }

                return typeList;
        }

        std::vector<TFrAbstractT> TFrame::Read(std::vector<TString> tagList, const std::type_index typeIndex)
        {
                this->Read();

                std::vector<TPRegexp> regex, antiRegex;
                for(int i = 0, N = tagList.size(); i < N; i++)
                { 
                        TString _tag = tagList[i].ReplaceAll("*", ".*");
                        if(_tag.BeginsWith("-")) antiRegex.push_back(TPRegexp("^"+_tag(1, _tag.Length()-1)+"$"));
                        else regex.push_back(TPRegexp("^"+_tag+"$"));
                }

                std::vector<TFrAbstractT> objList;
                for(int i = 0, I = this->GetNProxies(); i < I; i++) {

                        TString proxyClassType = std::visit([](auto* obj) -> TString { return obj->GetType(false); }, this->proxyMap[i]); 
                        if(typeIndex.name() != typeid(void).name() && !proxyClassType.EqualTo(typeIndex.name()))
                                continue;

                        auto _nBaskets = std::visit([](auto* obj) -> decltype(obj->GetNBaskets()) { return obj->GetNBaskets(); }, this->proxyMap[i]);
                        for(int basket = 0; basket < _nBaskets; basket++) {

                                auto _tag = std::visit([&basket](auto* obj) -> TString { return obj->GetName(basket); }, this->proxyMap[i]);
                                bool valid = true;
                                for(int j = 0; j < regex.size(); j++) {
                                        
                                        if(!valid) break;
                                        if(!regex[j].Match(_tag)) valid &= false;
                                }

                                for(int j = 0; j < antiRegex.size(); j++) {
                                        
                                        if(!valid) break;
                                        if(antiRegex[j].Match(_tag)) valid &= false;
                                }

                                if(!valid) continue;

                                auto _obj = std::visit([&basket](auto* obj) -> TFrAbstractT { return obj->Get(basket); }, this->proxyMap[i]);
                                objList.push_back(_obj);
                        }
                }

                return objList;
        }

        std::vector<TString> TFrame::GetDataTags() 
        { 
                return ROOT::IOPlus::Helpers::Explode(" ", this->tags);
        }

        void TFrame::Print(Option_t *opt)
        {
                TString option = opt;
                        option.ToLower();

                int N = this->fChain ? this->GetN() : 0;
                TString broken = !this->IsContiguous() ? TPrint::kRed + " [BROKEN]" + TPrint::kNoColor : "";

                if(option.Contains("interactive")) {

                        std::vector<TString> line(7);
                        int nFiles = this->fChain->GetNFiles();
                        int nFrames = this->fChain->GetNFrames(fileID);

                        if(this->fChain == NULL) line[1] = TPrint::kRed + "Frame (unchained)" + TPrint::kNoColor;
                        else {
                                line[1] = TPrint::kGreen + "Master #" + TString::Itoa(fileID+1,10) + "/" + TString::Itoa(nFiles,10) + TPrint::kNoColor + "; ";
                                line[1] += TPrint::kRed + "Frame #" + TString::Itoa(frameID+1,10)+ "/" + nFrames + TPrint::kNoColor + broken;
                        }

                        line[2] = "File location: " + TPrint::MakeItShorter(this->GetFileName(), 64) + TPrint::kNoColor;
                        line[3] = ">>> ranging from " + TPrint::kOrange + gClockGPS->UTC(this->GetGPS().first) + TPrint::kNoColor + " to " + TPrint::kOrange + gClockGPS->UTC(this->GetGPS().second) + TPrint::kNoColor;
                        line[4] = TString("    GPS ") + TPrint::kOrange + (int) this->GetGPS().first + TPrint::kNoColor + "; dt = " + TPrint::kOrange + Form("%.2f", (this->GetGPS().second-this->GetGPS().first)) + "s";
                        line[4] += TString("; ") + TPrint::kOrange + N + " object(s)" + TPrint::kNoColor;
                        line[4] += TString("; ") + "DQ " + TPrint::kOrange + "0x" + this->GetDataQuality() + TPrint::kNoColor;
                        
                        std::cout << std::endl;
                        std::cout << "    ____ ____  __  _  _ ____     "      << line[0] << std::endl;
                        std::cout << "   (  __(  _ \\/ _\\( \\/ (  __)    "   << line[1] << std::endl;
                        std::cout << "    ) _) )   /    / \\/ \\) _)     "    << line[2] << std::endl;
                        std::cout << "   (__)_(__\\_\\_/\\_\\_)(_(____)_   "  << line[3] << std::endl;
                        std::cout << "   (  __/  (  _ ( \\/ )/ _(_  _)  "     << line[4] << std::endl;
                        std::cout << "    ) _(  O )   / \\/ /    \\)(    "    << line[5] << std::endl;
                        std::cout << "   (__) \\__(__\\_\\_)(_\\_/\\_(__)   " << std::flush;

                } else {

                        if(this->fChain == NULL) std::cout << TPrint::kRed << "Frame (unchained)" << TPrint::kNoColor;
                        else {
                                
                                TString filename = TPrint::MakeItShorter(this->GetFileName(), 64);
                                int nFiles = this->fChain->GetNFiles();
                                int nFrames = this->fChain->GetNFrames(fileID);
                                
                                std::cout << std::dec << TPrint::kGreen << "Master #" << (fileID+1) << "/" << nFiles << TPrint::kNoColor << "; ";
                                std::cout << TPrint::kRed << "Frame #" << (frameID+1)<< "/" << nFrames << TPrint::kNoColor;
                                std::cout << "; " << filename << TPrint::kNoColor << "; ";
                        
                                int globalID = this->fChain->GetGlobalID(fileID, frameID);
                                int nTotal = this->fChain->GetNFrames();
                                std::cout << "ID " << TPrint::kOrange << (globalID+1) << "/" << nTotal << TPrint::kNoColor << "; ";
                        }

                        std::cout << "GPS " << std::fixed << TPrint::kOrange << (int) this->GetGPS().first << TPrint::kNoColor << std::dec;
                        std::cout << "; dt = " << TPrint::kOrange << Form("%.2f", this->GetGPS().second-this->GetGPS().first) << "s" << TPrint::kNoColor << std::dec;
                        std::cout << "; " << TPrint::kOrange << N << " object(s)" << TPrint::kNoColor << std::dec;
                        std::cout << "; " << "DQ "<< TPrint::kOrange << "0x" << std::hex << this->GetDataQuality() << TPrint::kNoColor << std::dec;
                        std::cout << broken << std::endl;
                }
        }

        std::vector<FrType> TFrame::GetProxyDataType()
        {
                std::vector<FrType> typeList;
                for(int i = 0, N = this->GetNProxies(); i < N; i++) {

                        FrType type;

                        TString typeStr = std::visit([](auto* obj) -> TString { return obj->GetType(false); }, this->proxyMap[i]); 
                        if(typeStr.EqualTo("FrameFormat::TFrAdcData"))
                                type = FrameFormat::FrType::FrAdcData;
                        if(typeStr.EqualTo("FrameFormat::TFrDetector"))
                                type = FrameFormat::FrType::FrDetector;
                        if(typeStr.EqualTo("FrameFormat::TFrEvent"))
                                type = FrameFormat::FrType::FrEvent;
                        if(typeStr.EqualTo("FrameFormat::TFrHistory"))
                                type = FrameFormat::FrType::FrHistory;
                        if(typeStr.EqualTo("FrameFormat::TFrMsg"))
                                type = FrameFormat::FrType::FrMsg;
                        if(typeStr.EqualTo("FrameFormat::TFrProcData"))
                                type = FrameFormat::FrType::FrProcData;
                        if(typeStr.EqualTo("FrameFormat::TFrRawData"))
                                type = FrameFormat::FrType::FrRawData;
                        if(typeStr.EqualTo("FrameFormat::TFrSerData"))
                                type = FrameFormat::FrType::FrSerData;
                        if(typeStr.EqualTo("FrameFormat::TFrSimData"))
                                type = FrameFormat::FrType::FrSimData;
                        if(typeStr.EqualTo("FrameFormat::TFrSimEvent"))
                                type = FrameFormat::FrType::FrSimEvent;
                        if(typeStr.EqualTo("FrameFormat::TFrStatData"))
                                type = FrameFormat::FrType::FrStatData;
                        if(typeStr.EqualTo("FrameFormat::TFrSummary"))
                                type = FrameFormat::FrType::FrSummary;
                        if(typeStr.EqualTo("FrameFormat::TFrTable"))
                                type = FrameFormat::FrType::FrTable;
                        if(typeStr.EqualTo("FrameFormat::TFrVect"))
                                type = FrameFormat::FrType::FrVect;

                        if(std::find(typeList.begin(), typeList.end(), type) == typeList.end() ) typeList.push_back(type);
                }

                return typeList;
        }

        void TFrame::Dump()
        {
                TString frame = "==========  " + TPrint::kNoColor + (this->ReadSimulationFlag() ? "SIMULATED " : "") + "DATA FRAME #" + TString::Itoa(this->frameID+1,10) + " IN FILE " + this->GetFileName() + TPrint::kPurple+"  ==========";
                TString spacer = ROOT::IOPlus::Helpers::Spacer(frame.Length()-11, '=');

                std::cout << TPrint::kPurple << std::endl;
                std::cout << spacer << std::endl;
                std::cout << frame << std::endl;
                std::cout << spacer << TPrint::kNoColor << std::endl;
                
                if (gPrint->IsDebugEnabled(10)) std::cout << std::endl;
                TPrint::PressAnyKeyToContinue(10, __METHOD_NAME__, TString("Frame #") + TString::Itoa(this->frameID+1, 10) + " in \"" + this->GetFileName() + "\" will be dumped.");
                
                FrameDump(this->GetManager().Read(&this->GetRHead(), this->fileID, this->frameID), stdout, TMath::Max(1, this->GetVerbosity()));
        }

        GPS TFrame::GetGPS()
        {
                return this->gps;
        }

        TFrame &TFrame::SetGPS(GPS gps)
        {
                this->gps = gps;
                return *this;
        }

        double TFrame::GetDuration()
        {
                return this->gps.second - this->gps.first;
        }

        TFrame &TFrame::SetDataQuality(unsigned int dataQuality)
        {
                this->dataQuality = dataQuality;
                return *this;
        }


        unsigned int TFrame::GetDataQuality()
        {
                return this->dataQuality;
        }

        TString TFrame::GetFileName()
        {
                return fChain->GetFileName(this->fileID);
        }

        std::vector<TFrameProxyT> TFrame::ReadProxy()
        {
                this->Read();
                return proxyMap;
        }

        bool TFrame::Ready()
        {
                return this->GetManager().Find(&this->GetRHead(), this->fileID, this->frameID);
        }

        void TFrame::Free()
        {
                this->GetManager().Free(&this->GetRHead(), this->fileID, this->frameID);
        }

        template <typename T>
        TFrameProxyT TFrame::ReadProxy(void *ptr)
        {
                this->ReadProxy<T>();

                std::vector<TFrameProxy<T> *> list;
                for(int i = 0, N = proxyMap.size(); i < N; i++) {

                        auto _basketID = std::visit([&ptr](auto* obj) -> decltype(obj->GetBasketID(ptr)) { return obj->GetBasketID(ptr); }, this->proxyMap[i]);
                        if(_basketID < 0) continue;

                        return proxyMap[i];
                }

                return TFrameProxyT();
        }

        template <typename T>
        std::vector<TFrameProxyT> TFrame::ReadProxy(std::vector<TString> tagList)
        {
                this->ReadProxy<T>();

                std::vector<TFrameProxyT> proxyMapList;
                for(int i = 0, I = this->proxyMap.size(); i < I; i++) {

                        auto _identifier = std::visit([](auto* obj) -> decltype(obj->GetIdentifier()) { return obj->GetIdentifier(); }, this->proxyMap[i]);
                        if(tagList.size() > 0 && std::find(tagList.begin(), tagList.end(), _identifier) == tagList.end()) continue;

                        proxyMapList.push_back(this->proxyMap[i]);
                }

                return proxyMapList;
        }

        TString TFrame::ReadName()
        {
                FrameH *frame = this->GetManager().Read(&this->GetRHead(), this->fileID, this->frameID);
                return TString(frame != NULL ? frame->name : "");
        }

        int TFrame::ReadRunNumber()
        {
                FrameH *frame = this->GetManager().Read(&this->GetRHead(), this->fileID, this->frameID);
                return frame != NULL ? frame->run : 0;
        }

        int TFrame::ReadFrameNumber()
        {
                FrameH *frame = this->GetManager().Read(&this->GetRHead(), this->fileID, this->frameID);
                return frame != NULL ? frame->frame : 0;
        }
        
        double TFrame::ReadLength()
        {
                FrameH *frame = this->GetManager().Read(&this->GetRHead(), this->fileID, this->frameID);
                return frame != NULL ? frame->dt : NAN;
        }

        bool TFrame::ReadSimulationFlag()
        {
                FrameH *frame = this->GetManager().Read(&this->GetRHead(), this->fileID, this->frameID);
                return frame->detectSim != NULL || frame->simEvent != NULL;
        }

        unsigned int TFrame::ReadGTimeS()
        {
                FrameH *frame = this->GetManager().Read(&this->GetRHead(), this->fileID, this->frameID);
                return frame != NULL ? frame->GTimeS : 0;
        }

        unsigned int TFrame::ReadGTimeN()
        {
                FrameH *frame = this->GetManager().Read(&this->GetRHead(), this->fileID, this->frameID);
                return frame != NULL ? frame->GTimeN : 0;
        }

        unsigned short TFrame::ReadULeapS()
        {
                FrameH *frame = this->GetManager().Read(&this->GetRHead(), this->fileID, this->frameID);
                return frame != NULL ? frame->ULeapS : 0;
        }
};
